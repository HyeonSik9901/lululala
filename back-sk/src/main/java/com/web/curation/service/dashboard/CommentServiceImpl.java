package com.web.curation.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.model.dashboard.Comment;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	private com.web.curation.dao.dashboard.CommentDao commentDao;
	
	
	@Override
	public boolean insertComment(Comment comment) throws Exception {
		return commentDao.insertComment(comment);
	}

	@Override
	public boolean updateComment(Comment comment) throws Exception {
		return commentDao.updateComment(comment);
	}

	@Override
	public boolean deleteComment(int id) throws Exception {
		return commentDao.deleteComment(id);
	}

	@Override
	public List<Comment> searchComment(int id) throws Exception {
		return commentDao.searchComment(id);
	}

}
