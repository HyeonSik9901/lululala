package com.web.curation.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.dashboard.CafeDao;
import com.web.curation.model.lala.Cafe;

@Service
public class CafeServiceImpl implements CafeService{
	
	@Autowired
	private CafeDao cafedao;
	
	
	@Override
	public boolean insertCafe(Cafe cafe) throws Exception {
		return cafedao.insertCafe(cafe);
	}

	@Override
	public boolean deleteCafe(int cafeid) throws Exception {
		return cafedao.deleteCafe(cafeid);
	}

	@Override
	public boolean updateCafe(Cafe cafe) throws Exception {
		return cafedao.updateCafe(cafe);
	}

	@Override
	public List<Cafe> searchCafeByTag(String tag) throws Exception {
		return cafedao.searchCafeByTag(tag);
	}

	@Override
	public List<Cafe> searchCafeByMyTag(int uid) throws Exception {
		return cafedao.searchCafeByMyTag(uid);
	}

	@Override
	public Cafe searchOneCafeById(int cafeid) throws Exception {
		return cafedao.searchOneCafeById(cafeid);
	}

	@Override
	public List<Cafe> rankBySearchCount() {
		return cafedao.rankBySearchCount();
	}

	@Override
	public List<Cafe> rankByMemberCount() {
		return cafedao.rankByMemberCount();
	}

	@Override
	public List<Cafe> searchAllCafe() {
		return cafedao.searchAllCafe();
	}

	@Override
	public List<Cafe> searchMyCafe(int uid) throws Exception {
		return cafedao.searchMyCafe(uid);
	}

	@Override
	public boolean countPlusCafe(Cafe cafe) throws Exception {
		return cafedao.countPlusCafe(cafe);
	}
	
}
