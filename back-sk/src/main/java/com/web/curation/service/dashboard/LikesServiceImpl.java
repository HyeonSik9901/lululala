package com.web.curation.service.dashboard;

import com.web.curation.dao.dashboard.*;
import com.web.curation.model.user.Likes;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

@Service
public class LikesServiceImpl implements LikesService {

    @Autowired
    LikesDao likesdao;

    @Override
    public int insertLikes(Likes likes) throws Exception {
        return likesdao.insertLikes(likes);
    }

    @Override
    public int deleteLikes(Likes likes) throws Exception {
        return likesdao.deleteLikes(likes);
    }

    @Override
    public int searchLikes(int fid) throws Exception {
        return likesdao.searchLikes(fid);
    }

    @Override
    public Likes checklikefid(Likes likes) throws Exception {
        return likesdao.checklikefid(likes);
    }

	@Override
	public boolean checkLikeMe(Likes likes) throws Exception {
		return likesdao.checkLikeMe(likes);
	}
  
}