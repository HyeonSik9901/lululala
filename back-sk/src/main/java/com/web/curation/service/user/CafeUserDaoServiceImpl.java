package com.web.curation.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.user.CafeUserDao;
import com.web.curation.model.lala.CafeUser;

@Service
public class CafeUserDaoServiceImpl implements CafeUserDaoService{

	@Autowired
	CafeUserDao cafeuserdao;
	
	@Override
	public boolean insertCafeUser(CafeUser cafeuser) throws Exception {
		return cafeuserdao.insertCafeUser(cafeuser);
	}

	@Override
	public boolean deleteCafeUser(int cafeuserid) throws Exception {
		return cafeuserdao.deleteCafeUser(cafeuserid);
	}

	@Override
	public List<CafeUser> searchCafeMember(int cafeid) throws Exception {
		return cafeuserdao.searchCafeMember(cafeid);
	}

	@Override
	public boolean updateCafeUser(CafeUser cafeuser) throws Exception {
		return cafeuserdao.updateCafeUser(cafeuser);
	}
	
}
