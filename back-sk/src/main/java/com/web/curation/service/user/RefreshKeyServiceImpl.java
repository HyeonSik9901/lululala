package com.web.curation.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.user.RefreshKeyDao;
import com.web.curation.model.user.RefreshKey;

@Service
public class RefreshKeyServiceImpl implements RefreshKeyService{
	
	@Autowired
	RefreshKeyDao refreshkeyDao;

	@Override
	public boolean insertRefreshKey(RefreshKey refreshkey) throws Exception {
		return refreshkeyDao.insertRefreshKey(refreshkey);
	}

	@Override
	public boolean deleteRefreshKey(RefreshKey refreshkey) throws Exception {
		return refreshkeyDao.deleteRefreshKey(refreshkey);
	}

	@Override
	public String searchRefreshKey(RefreshKey refreshKey) throws Exception {
		return refreshkeyDao.searchRefreshKey(refreshKey);
	}

	@Override
	public String searchuid(int uid) throws Exception {

		return refreshkeyDao.searchuid(uid);
	}
}
