//디비 셋팅 주석
package com.web.curation.service.user;

import java.sql.SQLException;
import java.util.List;

import com.web.curation.model.user.User;

public interface UserDaoService{
    User login(User user);

    int addUser(User user);

    User getUserByEmail(String email);

    User findUserByEmailAndPassword(String email, String password);
    
    boolean deleteUser(String email) throws SQLException;
    
    User searchUser(int identify) throws SQLException;
    List<User> searchNick(String nick) throws SQLException;
   	
    boolean updateUser(User member) throws SQLException;
    
    User findUserByEmail(String email) throws SQLException;
    User findUserByNick(String nickname) throws SQLException;
    boolean searchNickname(String nickname) throws Exception;
    User findUserByUid(int uid)throws Exception;
}