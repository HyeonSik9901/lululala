package com.web.curation.service.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.CafeFeed;

public interface CafeFeedService {
	
	public boolean insertCafeFeed(CafeFeed cafefeed)throws Exception;
	//카페 글쓰기
	public boolean updateCafeFeed(CafeFeed cafefeed)throws Exception;
	//카페 글 수정
	public boolean deleteCafeFeed(int cfid)throws Exception;
	//카페 글 삭제
	public List<CafeFeed> searchAllCafeFeed(int cafeid) throws Exception;	
	//카페 글 목록 출력
	
}
