package com.web.curation.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.user.QurationDao;
import com.web.curation.model.user.Quration;

@Service
public class QurationDaoServiceImpl implements QurationDaoService{
	
	@Autowired
	QurationDao qurationDao;

	@Override
	public boolean insertQuration(Quration quration) throws Exception {
		return qurationDao.insertQuration(quration);
	}

	@Override
	public boolean deleteQuration(Quration quration) throws Exception {
		return qurationDao.deleteQuration(quration);
	}

	@Override
	public boolean updateQuration(Quration quration) throws Exception {
		return qurationDao.updateQuration(quration);
	}

	@Override
	public List<Quration> searchAllQuration(int uid) throws Exception {
		return qurationDao.searchAllQuration(uid);
	}

	@Override
	public Quration searchOneQuration(Quration quration) throws Exception {
		return qurationDao.searchOneQuration(quration);
	}

	@Override
	public boolean checkQuration(Quration quration) throws Exception {
		return qurationDao.checkQuration(quration);
	}

	@Override
	public List<String> searchQuration(Quration quration) throws Exception {
		return qurationDao.searchQuration(quration);
	}	    
	
}
