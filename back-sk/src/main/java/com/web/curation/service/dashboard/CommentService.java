package com.web.curation.service.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.Comment;


public interface CommentService {

	public boolean insertComment(Comment comment)throws Exception;
	public boolean updateComment(Comment comment)throws Exception;
	public boolean deleteComment(int id)throws Exception;
	public List<Comment> searchComment(int id) throws Exception;
	
}
