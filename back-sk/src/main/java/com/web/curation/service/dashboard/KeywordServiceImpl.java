package com.web.curation.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.dashboard.KeywordDao;
import com.web.curation.model.dashboard.Keyword;
import com.web.curation.model.user.Quration;

@Service
public class KeywordServiceImpl implements KeywordService{

	@Autowired
	KeywordDao keywordDao;

	@Override
	public boolean insertKeyword(Quration keyword) throws Exception {
		return keywordDao.insertKeyword(keyword);
	}

	@Override
	public boolean deleteKeyword(int qid) throws Exception {
		return keywordDao.deleteKeyword(qid);
	}

	@Override
	public List<Keyword> searchKeyword(int qid) throws Exception {
		return keywordDao.searchKeyword(qid);
	}
	
}
