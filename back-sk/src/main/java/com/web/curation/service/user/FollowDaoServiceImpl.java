package com.web.curation.service.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.user.FollowDao;
import com.web.curation.model.user.Follow;
import com.web.curation.model.user.User;

@Service
public class FollowDaoServiceImpl implements FollowDaoService{

	@Autowired
	FollowDao followdao;
	
	@Override
	public boolean insertFollow(Follow follow) throws Exception {
		System.out.println(follow.toString()+"여기는서비스");
		return followdao.insertFollow(follow);
	}

	@Override
	public boolean deleteFollow(Follow follow) throws Exception {
		return followdao.deleteFollow(follow);
	}

	@Override
	public List<User> searchFollower(int followeruid) throws Exception {
		return followdao.searchFollower(followeruid);
	}

	@Override
	public List<User> searchFollowing(int followuid) throws Exception {
		return followdao.searchFollowing(followuid);
	}

}
