// 디비 셋팅 주석
package com.web.curation.service.user;

import com.web.curation.dao.user.UserDao;
import com.web.curation.model.user.User;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDaoServiceImpl implements UserDaoService {

    @Autowired
    UserDao userdao;

    @Override
    public User findUserByEmailAndPassword(String email, String password) {
        return null;
    }

    @Override
    public User getUserByEmail(String email) {

        return null;
    }

    @Override
    public int addUser(User user) {

        return userdao.addUser(user);
    }

    @Override
    public boolean deleteUser(String email) throws SQLException {
        return userdao.deleteUser(email);
    }

    @Override
    public User searchUser(int identify) throws SQLException {
        return userdao.searchUser(identify);
    }

    @Override
    public boolean updateUser(User user) throws SQLException {
        return userdao.updateUser(user);
    }

    @Override
    public User login(User user) {
        return userdao.login(user);
    }

    @Override
    public List<User> searchNick(String nick) throws SQLException {
        return userdao.searchNick(nick);
    }

    @Override
    public User findUserByEmail(String email) throws SQLException {

        return userdao.findUserByEmail(email);
    }

	@Override
	public boolean searchNickname(String nickname) throws Exception {
		return userdao.searchNickname(nickname);
	}

    @Override
    public User findUserByNick(String nickname) throws SQLException {
        return userdao.findUserByNick(nickname);
    }

	@Override
	public User findUserByUid(int uid) throws Exception {
		return userdao.findUserByUid(uid);
	}

    
}