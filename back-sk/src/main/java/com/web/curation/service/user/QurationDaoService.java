package com.web.curation.service.user;

import java.util.List;

import com.web.curation.model.user.Quration;

public interface QurationDaoService {

	public boolean insertQuration(Quration quration)throws Exception;
	
	public boolean deleteQuration(Quration quration)throws Exception;
	
	public boolean updateQuration(Quration quration)throws Exception;
	
	public List<Quration> searchAllQuration(int uid)throws Exception;
	
	public Quration searchOneQuration(Quration quration)throws Exception;
	
	public boolean checkQuration(Quration quration)throws Exception;

	public List<String> searchQuration(Quration quration)throws Exception;
}
