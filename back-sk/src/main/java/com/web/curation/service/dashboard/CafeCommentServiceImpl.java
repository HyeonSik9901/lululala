package com.web.curation.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.dashboard.CafeCommentDao;
import com.web.curation.model.dashboard.CafeComment;
import com.web.curation.model.dashboard.Comment;

@Service
public class CafeCommentServiceImpl implements CafeCommentService{

	@Autowired
	private CafeCommentDao cafecommentDao;
	
	
	@Override
	public boolean insertCafeComment(CafeComment cafecomment) throws Exception {
		return cafecommentDao.insertCafeComment(cafecomment);
	}

	@Override
	public boolean updateCafeComment(CafeComment cafecomment) throws Exception {
		return cafecommentDao.updateCafeComment(cafecomment);
	}

	@Override
	public boolean deleteCafeComment(int id) throws Exception {
		return cafecommentDao.deleteCafeComment(id);
	}

	@Override
	public List<CafeComment> searchCafeComment(int id) throws Exception {
		return cafecommentDao.searchCafeComment(id);
	}

}
