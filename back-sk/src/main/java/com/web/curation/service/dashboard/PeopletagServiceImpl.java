package com.web.curation.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.dashboard.PeopletagDao;
import com.web.curation.model.dashboard.Peopletag;

@Service
public class PeopletagServiceImpl implements PeopletagService{
	
	@Autowired
	PeopletagDao peopletagDao;
	
	@Override
	public boolean insertPeopletag(Peopletag peopletag) throws Exception {
		return peopletagDao.insertPeopletag(peopletag);
	}

	@Override
	public boolean deletePeopletag(Peopletag peopletag) throws Exception {
		return peopletagDao.deletePeopletag(peopletag);
	}

	@Override
	public List<Peopletag> searchPeopletag(int fid) throws Exception {
		return peopletagDao.searchPeopletag(fid);
	}

	@Override
	public boolean searchPeopleByCheck(String people) throws Exception {
		return peopletagDao.searchPeopleByCheck(people);
	}

	@Override
	public boolean deletePeopletagByFeed(int fid) throws Exception {
		return peopletagDao.deletePeopletagByFeed(fid);
	}

}
