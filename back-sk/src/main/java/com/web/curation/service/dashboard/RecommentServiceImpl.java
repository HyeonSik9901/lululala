package com.web.curation.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.dashboard.RecommentDao;
import com.web.curation.model.dashboard.Recomment;

@Service
public class RecommentServiceImpl implements RecommentService{

	@Autowired
	private RecommentDao recommentDao;

	@Override
	public boolean insertRecomment(Recomment recomment) throws Exception {
		return recommentDao.insertRecomment(recomment);
	}

	@Override
	public boolean updateRecomment(Recomment recomment) throws Exception {
		return recommentDao.updateRecomment(recomment);
	}

	@Override
	public boolean deleteRecomment(int recid) throws Exception {
		return recommentDao.deleteRecomment(recid);
	}

	@Override
	public List<Recomment> searchRecomment(int cid) throws Exception {
		return recommentDao.searchRecomment(cid);
	}

	@Override
	public int countRecomment(int cid) throws Exception {
		return recommentDao.countRecomment(cid);
	}
	
}
