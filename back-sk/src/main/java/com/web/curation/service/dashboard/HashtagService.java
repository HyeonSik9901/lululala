package com.web.curation.service.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.Hashtag;

public interface HashtagService {

	public boolean insertHashtag(String hashtag)throws Exception;
	
	public boolean deleteHashtag(Hashtag hashtag)throws Exception;
	
	//업데이트 없음 해쉬태그는 삭제하고 추가하고 팔로우랑 똑같음.
	
	public List<Hashtag> searchHashtag(int fid)throws Exception;
	
	public boolean searchHashtagByCheck(String tag)throws Exception;
}
