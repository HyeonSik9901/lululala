package com.web.curation.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.dashboard.CafeFeedDao;
import com.web.curation.model.dashboard.CafeFeed;

@Service
public class CafeFeedServiceImpl implements CafeFeedService{

	@Autowired
	private CafeFeedDao cafefeeddao;

	@Override
	public boolean insertCafeFeed(CafeFeed cafefeed) throws Exception {
		return cafefeeddao.insertCafeFeed(cafefeed);
	}

	@Override
	public boolean updateCafeFeed(CafeFeed cafefeed) throws Exception {
		return cafefeeddao.updateCafeFeed(cafefeed);
	}

	@Override
	public boolean deleteCafeFeed(int cfid) throws Exception {
		return cafefeeddao.deleteCafeFeed(cfid);
	}

	@Override
	public List<CafeFeed> searchAllCafeFeed(int cafeid) throws Exception {
		return cafefeeddao.searchAllCafeFeed(cafeid);
	
	}


}
