package com.web.curation.service.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.CafeComment;


public interface CafeCommentService {

	public boolean insertCafeComment(CafeComment cafecomment)throws Exception;
	public boolean updateCafeComment(CafeComment cafecomment)throws Exception;
	public boolean deleteCafeComment(int id)throws Exception;
	public List<CafeComment> searchCafeComment(int id) throws Exception;
	
}
