package com.web.curation.service.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.Keyword;
import com.web.curation.model.user.Quration;


public interface KeywordService {

	public boolean insertKeyword(Quration keyword)throws Exception;
	
	public boolean deleteKeyword(int qid)throws Exception;
	
	public List<Keyword> searchKeyword(int kid)throws Exception;


}
