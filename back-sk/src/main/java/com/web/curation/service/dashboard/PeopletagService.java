package com.web.curation.service.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.Peopletag;

public interface PeopletagService {
	
	public boolean insertPeopletag(Peopletag peopletag) throws Exception;

	public boolean deletePeopletag(Peopletag peopletag) throws Exception;

	public List<Peopletag> searchPeopletag(int fid) throws Exception;
	
	public boolean searchPeopleByCheck(String people)throws Exception;
	
	public boolean deletePeopletagByFeed(int fid)throws Exception;
}
