package com.web.curation.service.dashboard;

import com.web.curation.model.user.Likes;

public interface LikesService {
    public int insertLikes(Likes likes)throws Exception;
    public int deleteLikes(Likes likes)throws Exception;
    public int searchLikes(int fid)throws Exception;
    public Likes checklikefid(Likes likes)throws Exception;
	
    public boolean checkLikeMe(Likes likes)throws Exception; //내가 좋아요 했는지 안했는지 여부
}
