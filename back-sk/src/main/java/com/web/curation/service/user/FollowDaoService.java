package com.web.curation.service.user;

import java.util.List;

import com.web.curation.model.user.Follow;
import com.web.curation.model.user.User;

public interface FollowDaoService {
	public boolean insertFollow(Follow follow)throws Exception;
	public boolean deleteFollow(Follow follow)throws Exception;
	public List<User> searchFollower(int followeruid)throws Exception;
	public List<User> searchFollowing(int followuid)throws Exception;
	
	//팔로우를 추가하거나, 팔로우를 삭제하거나, 찾는다.
}
