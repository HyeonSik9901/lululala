package com.web.curation.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.dashboard.FeedDao;
import com.web.curation.model.dashboard.Feed;
import com.web.curation.model.dashboard.Feedtag;
import com.web.curation.model.dashboard.Peopletag;

@Service
public class FeedServiceImpl implements FeedService {

	@Autowired
	private FeedDao feedDao;
	
	@Override
	public boolean insertFeed(Feed feed) throws Exception {
		return feedDao.insertFeed(feed);
	}

	@Override
	public boolean updateFeed(Feed feed) throws Exception {
		return feedDao.updateFeed(feed);
	}

	@Override
	public boolean deleteFeed(int fid) throws Exception {
		return feedDao.deleteFeed(fid);
	}

	@Override
	public List<Feed> searchAllFeed(int uid) throws Exception {
		return feedDao.searchAllFeed(uid);
	}

	@Override
	public List<Feed> searchFeedByLike(int uid) throws Exception {
		return feedDao.searchFeedByLike(uid);
	}

	@Override
	public List<Feed> searchFeedByfollow(int uid) throws Exception {
		return feedDao.searchFeedByfollow(uid);
	}

	@Override
	public List<Feed> searchFeedByHashtag(int uid) throws Exception {
		return feedDao.searchFeedByHashtag(uid);
	}

	@Override
	public List<Feed> searchPageFeed(int pageNum) throws Exception {
		return feedDao.searchPageFeed(pageNum);
	}

	@Override
	public int LikeCountFeed(int fid) throws Exception {
		return feedDao.LikeCountFeed(fid);
	}

	@Override
	public int CommentCountFeed(int fid) throws Exception {
		return feedDao.CommentCountFeed(fid);
	}

	@Override
	public int searchFidBywritedate(Feed feed) throws Exception {
		return feedDao.searchFidBywritedate(feed);
	}

	@Override
	public int insertFeedTag(Feedtag feedtag) throws Exception {
		return feedDao.insertFeedTag(feedtag);
	}
	
	@Override
	public int insertPeopletag(Peopletag peopletag) throws Exception {
		return feedDao.insertPeopleTag(peopletag);
	}

	@Override
	public boolean deleteFeedTag(int fid) throws Exception {
		return feedDao.deleteFeedTag(fid);
	}

	@Override
	public List<Feed> searchAllmyFeed(int uid) throws Exception {
		return feedDao.searchAllmyFeed(uid);
	}

	@Override
	public List<Feed> searchFeedByPeopletag(int uid) throws Exception {
		return feedDao.searchFeedByPeopletag(uid);
	}

	@Override
	public List<String> searchHashTagByFid(int fid) throws Exception {
		return feedDao.searchHashTagByFid(fid);
	}

	@Override
	public List<String> searchPeopleTagByFid(int fid) throws Exception {
		return feedDao.searchPeopleTagByFid(fid);
	}

	@Override
	public List<Feed> searchFeedByKeyword(String keyword) throws Exception {
		return feedDao.searchFeedByKeyword(keyword);
	}
}
