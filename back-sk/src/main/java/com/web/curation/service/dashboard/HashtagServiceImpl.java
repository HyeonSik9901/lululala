package com.web.curation.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.dashboard.HashtagDao;
import com.web.curation.model.dashboard.Hashtag;

@Service
public class HashtagServiceImpl implements HashtagService{

	@Autowired
	HashtagDao hashtagDao;
	
	@Override
	public boolean insertHashtag(String hashtag) throws Exception {
		return hashtagDao.insertHashtag(hashtag);
	}

	@Override
	public boolean deleteHashtag(Hashtag hashtag) throws Exception {
		return hashtagDao.deleteHashtag(hashtag);
	}

	@Override
	public List<Hashtag> searchHashtag(int fid) throws Exception {
		return hashtagDao.searchHashtag(fid);
	}

	@Override
	public boolean searchHashtagByCheck(String tag) throws Exception {
		return hashtagDao.searchHashtagByCheck(tag);
	}

}
