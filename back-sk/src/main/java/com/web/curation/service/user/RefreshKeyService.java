package com.web.curation.service.user;

import com.web.curation.model.user.RefreshKey;

public interface RefreshKeyService {

	public boolean insertRefreshKey(RefreshKey refreshkey)throws Exception;
	
	public boolean deleteRefreshKey(RefreshKey refreshkey)throws Exception;

	public String searchRefreshKey(RefreshKey refreshKey) throws Exception;
	//유저 아이디를 이용해서 리프레시 키 만 발급 받는다.
	public String searchuid(int uid) throws Exception;
}
