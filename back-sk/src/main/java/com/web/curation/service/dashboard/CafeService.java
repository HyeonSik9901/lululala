package com.web.curation.service.dashboard;

import java.util.List;

import com.web.curation.model.lala.Cafe;

public interface CafeService {
	
	public boolean insertCafe(Cafe cafe)throws Exception;
	// 카페 만들기
	public boolean deleteCafe(int cafeid)throws Exception;
	// 카페 삭제하기
	public boolean updateCafe(Cafe cafe)throws Exception;
	// 카페 이름, 한줄소개 , 카페 프로필 이미지 수정하기
	public List<Cafe> searchCafeByTag(String tag)throws Exception;
	// 태그들 넣어서 그 태그를 포함하는 카페 목록
	public List<Cafe> searchCafeByMyTag(int uid)throws Exception;
	// 내 태그와 비교해서 추천하는 카페목록
	public Cafe searchOneCafeById(int cafeid)throws Exception;
	// 카페 id로 카페 하나의 모든 정보 찾아오기(회원수 여기서 가져오면됨 , 조회수 1증가)
	public List<Cafe> rankBySearchCount();
	// 카페 조회수 별로 랭킹 순위
	public List<Cafe> rankByMemberCount();
	// 카페회원수 별로 순위 부르는 로직 
	public List<Cafe> searchAllCafe();
	// 전체 카페 목록
	public List<Cafe> searchMyCafe(int uid)throws Exception;
	// 내가 가입되어있는 카페 목록
	public boolean countPlusCafe(Cafe cafe)throws Exception;
	//카페 조회수 +1 해주는 메소드 
}
