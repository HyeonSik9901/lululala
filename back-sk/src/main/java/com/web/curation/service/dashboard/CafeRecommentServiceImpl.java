package com.web.curation.service.dashboard;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.curation.dao.dashboard.CafeRecommentDao;
import com.web.curation.dao.dashboard.RecommentDao;
import com.web.curation.model.dashboard.CafeRecomment;
import com.web.curation.model.dashboard.Recomment;

@Service
public class CafeRecommentServiceImpl implements CafeRecommentService{

	@Autowired
	private CafeRecommentDao caferecommentDao;

	@Override
	public boolean insertCafeRecomment(CafeRecomment caferecomment) throws Exception {
		return caferecommentDao.insertCafeRecomment(caferecomment);
	}

	@Override
	public boolean updateCafeRecomment(CafeRecomment caferecomment) throws Exception {
		return caferecommentDao.updateCafeRecomment(caferecomment);
	}

	@Override
	public boolean deleteCafeRecomment(int recid) throws Exception {
		return caferecommentDao.deleteCafeRecomment(recid);
	}

	@Override
	public List<CafeRecomment> searchCafeRecomment(int cid) throws Exception {
		return caferecommentDao.searchCafeRecomment(cid);
	}

	@Override
	public int countCafeRecomment(int cid) throws Exception {
		return caferecommentDao.countCafeRecomment(cid);
	}
	
}
