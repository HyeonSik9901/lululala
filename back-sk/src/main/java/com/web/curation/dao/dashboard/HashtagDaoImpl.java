package com.web.curation.dao.dashboard;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.dashboard.Hashtag;

@Repository
public class HashtagDaoImpl implements HashtagDao{

	private String mapper = "HashtagMapper.";
	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public boolean insertHashtag(String hashtag) throws Exception {
		int cnt = sqlSession.insert(mapper+"insertHashtag",hashtag);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteHashtag(Hashtag hashtag) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteHashtag",hashtag);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Hashtag> searchHashtag(int fid) throws Exception {
		return sqlSession.selectList(mapper+"searchHashtag",fid);
	}

	@Override
	public boolean searchHashtagByCheck(String tag) throws Exception {
		int cnt = sqlSession.selectOne(mapper+"searchHashtagByCheck",tag);
		if(cnt!=0) {
			return false;
		}
		return true;
	}
}
