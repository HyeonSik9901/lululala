package com.web.curation.dao.user;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.user.Quration;

@Repository
public class QurationDaoImpl implements QurationDao{

	@Autowired
	SqlSession sqlSession;
	
	private String mapper = "QurationMapper.";
	
	@Override
	public boolean insertQuration(Quration quration) throws Exception {
		int cnt = sqlSession.insert(mapper+"insertQuration",quration);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteQuration(Quration quration) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteQuration",quration);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateQuration(Quration quration) throws Exception {
		int cnt = sqlSession.update(mapper+"updateQuration",quration);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Quration> searchAllQuration(int uid) throws Exception {
		return sqlSession.selectList(mapper+"searchAllQuration",uid);
	}

	@Override
	public Quration searchOneQuration(Quration quration) throws Exception {
		return sqlSession.selectOne(mapper+"searchOneQuration",quration);
	}

	@Override
	public boolean checkQuration(Quration quration) throws Exception {
		int cnt = sqlSession.selectOne(mapper+"checkQuration",quration);
		
		if(cnt!=0) {
			return true;	//있다는것 true
		}
		return false;	//없다
	}

	@Override
	public List<String> searchQuration(Quration quration) throws Exception {
		return sqlSession.selectList(mapper+"searchQuration",quration);
	}
	
	

}
