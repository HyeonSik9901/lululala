package com.web.curation.dao.dashboard;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.dashboard.Recomment;

@Repository
public class RecommentDaoImpl implements RecommentDao{

	@Autowired
	private SqlSession sqlSession;

	private String mapper = "recommentMapper.";

	@Override
	public boolean insertRecomment(Recomment recomment) throws Exception {
		int cnt =sqlSession.insert(mapper+"insertRecomment",recomment);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateRecomment(Recomment recomment) throws Exception {
		int cnt = sqlSession.update(mapper+"updateRecomment",recomment);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteRecomment(int recid) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteRecomment",recid);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Recomment> searchRecomment(int cid) throws Exception {
		return sqlSession.selectList(mapper+"searchRecomment",cid);
	}

	@Override
	public int countRecomment(int cid) throws Exception {
		return sqlSession.selectOne(mapper+"countRecomment",cid);
	}
	
	
}
