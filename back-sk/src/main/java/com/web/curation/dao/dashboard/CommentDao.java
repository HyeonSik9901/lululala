package com.web.curation.dao.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.Comment;


public interface CommentDao {
	
	public boolean insertComment(Comment comment)throws Exception;
	public boolean updateComment(Comment comment)throws Exception;
	public boolean deleteComment(int id)throws Exception;
	public List<Comment> searchComment(int id) throws Exception;

}
