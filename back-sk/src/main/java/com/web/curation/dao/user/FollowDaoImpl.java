package com.web.curation.dao.user;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.user.Follow;
import com.web.curation.model.user.User;

@Repository
public class FollowDaoImpl implements FollowDao{

	@Autowired
	SqlSession sqlSession;
	
	private String mapper = "followMapper.";
	
	@Override
	public boolean insertFollow(Follow follow) throws Exception {

		int cnt = sqlSession.insert(mapper+"insertFollow",follow);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteFollow(Follow follow) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteFollow",follow);
		if(cnt!=0) {
			return true;
		}
		return false;
	}
	
	@Override
	public List<User> searchFollower(int followeruid) throws Exception {
		return sqlSession.selectList(mapper+"searchFollower",followeruid);
	}

	@Override
	public List<User> searchFollowing(int followuid) throws Exception {
		return sqlSession.selectList(mapper+"searchFollowing",followuid);
	}
}
