package com.web.curation.dao.user;

import java.util.List;

import com.web.curation.model.user.Quration;


public interface QurationDao {

	public boolean insertQuration(Quration quration)throws Exception;
	
	public boolean deleteQuration(Quration quration)throws Exception;
	
	public boolean updateQuration(Quration quration)throws Exception;
	
	public List<Quration> searchAllQuration(int uid)throws Exception;
	
	public Quration searchOneQuration(Quration quration)throws Exception;
	//q레이션은 업데이트 가능으로 넣음 이름 수정이 가능해야함.
	
	public boolean checkQuration(Quration quration)throws Exception;
	
	public List<String> searchQuration(Quration quration)throws Exception;
}
