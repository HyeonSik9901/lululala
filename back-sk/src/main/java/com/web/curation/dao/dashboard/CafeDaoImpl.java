package com.web.curation.dao.dashboard;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.lala.Cafe;

@Repository
public class CafeDaoImpl implements CafeDao{

	private String mapper = "CafeMapper.";
	@Autowired
	private SqlSession sqlSession;
	
	
	@Override
	public boolean insertCafe(Cafe cafe) throws Exception {
		int cnt = sqlSession.insert(mapper+"insertCafe",cafe);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteCafe(int cafeid) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteCafe",cafeid);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateCafe(Cafe cafe) throws Exception {
		int cnt = sqlSession.update(mapper+"updateCafe",cafe);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Cafe> searchCafeByTag(String tag) throws Exception {
		return sqlSession.selectList(mapper+"searchCafeByTag",tag);
	}

	@Override
	public List<Cafe> searchCafeByMyTag(int uid) throws Exception {
		return sqlSession.selectList(mapper+"searchCafeByMyTag",uid);
	}

	@Override
	public Cafe searchOneCafeById(int cafeid) throws Exception {
		return sqlSession.selectOne(mapper+"searchOneCafeById",cafeid);
	}

	@Override
	public List<Cafe> rankBySearchCount() {
		return sqlSession.selectList(mapper+"rankBySearchCount");
	}

	@Override
	public List<Cafe> rankByMemberCount() {
		return sqlSession.selectList(mapper+"rankByMemberCount");
	}

	@Override
	public List<Cafe> searchAllCafe() {
		return sqlSession.selectList(mapper+"searchAllCafe");
	}

	@Override
	public List<Cafe> searchMyCafe(int uid) throws Exception {
		return sqlSession.selectList(mapper+"searchMyCafe",uid);
	}

	@Override
	public boolean countPlusCafe(Cafe cafe) throws Exception {
		int cnt = sqlSession.update(mapper+"countPlusCafe",cafe);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

}
