package com.web.curation.dao.user;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.user.RefreshKey;

@Repository
public class RefreshKeyDaoImpl implements RefreshKeyDao{

	@Autowired
	SqlSession sqlSession;
	private String mapper = "RefreshKeyMapper.";
	@Override
	public boolean insertRefreshKey(RefreshKey refreshkey) throws Exception {
		int cnt = sqlSession.insert(mapper+"insertRefreshKey",refreshkey);
		if(cnt!=0) {
			return true;
		}
		return false;
	}
	@Override
	public boolean deleteRefreshKey(RefreshKey refreshkey) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteRefreshKey",refreshkey);
		if(cnt!=0) {
			return true;
		}
		return false;
	}
	@Override
	public String searchRefreshKey(RefreshKey refreshKey) throws Exception {
		String cnt = sqlSession.selectOne(mapper+"searchRefreshKey",refreshKey);
		return cnt;
	}

	@Override
	public String searchuid(int uid) throws Exception {
		return sqlSession.selectOne(mapper+"searchuid",uid);
	}
	
	
}
