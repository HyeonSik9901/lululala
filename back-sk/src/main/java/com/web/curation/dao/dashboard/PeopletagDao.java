package com.web.curation.dao.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.Peopletag;


public interface PeopletagDao {

	public boolean insertPeopletag(Peopletag peopletag)throws Exception;
	
	public boolean deletePeopletag(Peopletag peopletag)throws Exception;
	
	public List<Peopletag> searchPeopletag(int fid)throws Exception;

	//1번 피드에 태그된 uid 1  1번 피드에 태그된 uid 3;
	
	public boolean searchPeopleByCheck(String people)throws Exception;
	
	public boolean deletePeopletagByFeed(int fid)throws Exception;
}
