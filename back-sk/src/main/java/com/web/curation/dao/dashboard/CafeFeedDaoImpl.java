package com.web.curation.dao.dashboard;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.dashboard.CafeFeed;
import com.web.curation.model.dashboard.Feed;

@Repository
public class CafeFeedDaoImpl implements CafeFeedDao{

	private String mapper = "CafeFeedMapper.";
	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public boolean insertCafeFeed(CafeFeed cafefeed) throws Exception {
		int cnt = sqlSession.insert(mapper+"insertCafeFeed",cafefeed);
		if(cnt!=0) {
			return true;
		}
		return false;
	}
	@Override
	public boolean updateCafeFeed(CafeFeed cafefeed) throws Exception {
		int cnt = sqlSession.update(mapper+"updateCafeFeed",cafefeed);
		if(cnt!=0) {
			return true;
		}
		return false;
	}
	@Override
	public boolean deleteCafeFeed(int cfid) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteCafeFeed",cfid);
		if(cnt!=0) {
			return true;
		}
		return false;
	}
	@Override
	public List<CafeFeed> searchAllCafeFeed(int cafeid) throws Exception {
		return sqlSession.selectList(mapper+"searchAllCafeFeed",cafeid);
	}
	
	
}
