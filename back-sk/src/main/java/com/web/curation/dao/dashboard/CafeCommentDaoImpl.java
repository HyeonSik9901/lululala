package com.web.curation.dao.dashboard;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.dashboard.CafeComment;
import com.web.curation.model.dashboard.Comment;

@Repository
public class CafeCommentDaoImpl implements CafeCommentDao{

	private String mapper = "cafecommentMapper.";
	@Autowired
	private SqlSession sqlSession;
	
	
	@Override
	public boolean insertCafeComment(CafeComment cafecomment) throws Exception {
		int cnt =sqlSession.insert(mapper+"insertCafeComment",cafecomment);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateCafeComment(CafeComment cafecomment) throws Exception {
		int cnt = sqlSession.update(mapper+"updateCafeComment",cafecomment);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteCafeComment(int id) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteCafeComment",id);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<CafeComment> searchCafeComment(int id) throws Exception {
		return sqlSession.selectList(mapper+"searchCafeComment",id);
	}
}
