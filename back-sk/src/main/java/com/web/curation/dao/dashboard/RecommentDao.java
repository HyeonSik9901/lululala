package com.web.curation.dao.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.Recomment;


public interface RecommentDao {

	public boolean insertRecomment(Recomment recomment)throws Exception;

	public boolean updateRecomment(Recomment recomment)throws Exception;
	
	public boolean deleteRecomment(int recid)throws Exception;
	
	public List<Recomment> searchRecomment(int cid) throws Exception;

	public int countRecomment (int cid) throws Exception;
}
