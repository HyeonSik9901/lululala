package com.web.curation.dao.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.Keyword;
import com.web.curation.model.user.Quration;


public interface KeywordDao {

	public boolean insertKeyword(Quration keyword)throws Exception;
	
	public boolean deleteKeyword(int qid)throws Exception;
	
	//키워드 1 큐레이션 번호 2번 tag 강아지
	//키워드 2 큐레이션 번호 2번 tag 고양이
	//여러개 키워드는 같은번호 큐레이션에 여러개 태그 가능
	public List<Keyword> searchKeyword(int qid)throws Exception;
	
	
}
