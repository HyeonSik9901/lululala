package com.web.curation.dao.dashboard;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.dashboard.Peopletag;

@Repository
public class PeopletagDaoImpl implements PeopletagDao{

	private String mapper = "PeopletagMapper.";
	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public boolean insertPeopletag(Peopletag peopletag) throws Exception {
		int cnt = sqlSession.insert(mapper+"insertPeopletag", peopletag);
		if(cnt!=0) {
			return true;
		}
		return false;
	}
	@Override
	public boolean deletePeopletag(Peopletag peopletag) throws Exception {
		int cnt = sqlSession.delete(mapper+"deletePeopletag", peopletag);
		if(cnt!=0) {
			return true;
		}
		return false;
	}
	
	@Override
	public List<Peopletag> searchPeopletag(int fid) throws Exception {
		return sqlSession.selectList(mapper+"searchPeopletag", fid);
	}
	
	@Override
	public boolean searchPeopleByCheck(String people) throws Exception {
		int cnt = sqlSession.selectOne(mapper+"searchPeopleByCheck",people);
		if(cnt!=0) {
			return true; 
		}
		return false;
	}
	
	@Override
	public boolean deletePeopletagByFeed(int fid) throws Exception {
		int cnt = sqlSession.delete(mapper+"deletePeopletagByFeed", fid);
		if(cnt!=0) {
			return true;
		}
		return false;
	}
	
}
