package com.web.curation.dao.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.CafeComment;


public interface CafeCommentDao {
	
	public boolean insertCafeComment(CafeComment cafecomment)throws Exception;
	public boolean updateCafeComment(CafeComment cafecomment)throws Exception;
	public boolean deleteCafeComment(int id)throws Exception;
	public List<CafeComment> searchCafeComment(int id) throws Exception;

}
