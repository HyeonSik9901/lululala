package com.web.curation.dao.dashboard;

import com.web.curation.model.user.Likes;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LikesDaoImpl implements LikesDao {

    @Autowired
    SqlSession sqlSession;

    String mapper = "LikesMapper.";

    @Override
    public int insertLikes(Likes likes) throws Exception {  
        return sqlSession.insert(mapper+"insertLikes",likes);
    }

    @Override
    public int deleteLikes(Likes likes) throws Exception {
        return sqlSession.delete(mapper+"deleteLikes", likes);
    }

    @Override
    public int searchLikes(int fid) throws Exception {
        return sqlSession.selectOne(mapper+"searchLikes",fid);
    }

    @Override
    public Likes checklikefid(Likes likes) throws Exception {
        return sqlSession.selectOne(mapper+"checklikefid",likes);
    }

	@Override
	public boolean checkLikeMe(Likes likes) throws Exception {
		int cnt = sqlSession.selectOne(mapper+"checkLikeMe",likes);
		if(cnt!=0) {
			return true;
		}
		return false;
	}
    
}