// 디비 셋팅 주석
package com.web.curation.dao.user;

import com.web.curation.model.user.User;

import java.sql.SQLException;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

//import org.springframework.beans.factory.annotation.Autowire;

import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	SqlSession sqlSession;

	@Override
	public User findUserByEmailAndPassword(String email, String password) {
		return null;
	}



	@Override
	public int addUser(User user) {

		return sqlSession.insert("userMapper.addUser", user);
	}

	@Override
	public boolean deleteUser(String email) throws SQLException {

		int flag = sqlSession.delete("userMapper.deleteUser", email);
		if (flag != 0) {
			return true; // 삭제성공
		} else
			return false; //
	}

	@Override
	public User searchUser(int identify) throws SQLException {
		return sqlSession.selectOne("userMapper.searchUser", identify);
	}

	@Override
	public boolean updateUser(User user) throws SQLException {
		System.out.println(user);

		int cnt = sqlSession.update("userMapper.updateUser", user);
		if (cnt != 0)
			return true; // 업데이트 성공
		else
			return false;
	}

	@Override
	public User login(User user) {
		User user1 = sqlSession.selectOne("userMapper.login", user);
		if (user1 == null) {
			user1 = new User();
			user1.setUid(0);
		}
		return user1;

	}

	@Override
	public List<User> searchNick(String nick) throws SQLException {

		return sqlSession.selectList("userMapper.searchNick",nick);
	}

	@Override
	public User findUserByEmail(String email) throws SQLException {

		return sqlSession.selectOne("userMapper.findUserByEmail",email);
	}



	@Override
	public boolean searchNickname(String nickname) throws Exception {
		int cnt = sqlSession.selectOne("userMapper.searchNickname",nickname);
		if(cnt!=0) {
			//같은게 있다는뜻 
			return false;
		}
		return true;
	}

	@Override
	public User findUserByNick(String nickname) throws SQLException {
		return sqlSession.selectOne("userMapper.findUserByNick",nickname);
	}



	@Override
	public User findUserByUid(int uid) throws Exception {
		return sqlSession.selectOne("userMapper.findUserByUid",uid);
	}

    


    
}