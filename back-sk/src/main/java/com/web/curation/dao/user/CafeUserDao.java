package com.web.curation.dao.user;

import java.util.List;

import com.web.curation.model.lala.CafeUser;

public interface CafeUserDao {

	public boolean insertCafeUser(CafeUser cafeuser)throws Exception;
	//카페 가입하기 >> 가입시 기본 내 정보의 프로필 이미지와 닉네임 들고온다.
	
	public boolean deleteCafeUser(int cafeuserid)throws Exception;
	//카페 탈퇴하기
	
	public List<CafeUser> searchCafeMember(int cafeid)throws Exception;
	//가입한 카페 회원 목록
	
	public boolean updateCafeUser(CafeUser cafeuser)throws Exception;
	//카페 닉네임 프로필 사진 수정
	
}
