package com.web.curation.dao.dashboard;

import com.web.curation.model.user.Likes;

public interface LikesDao {
    public int insertLikes(Likes likes)throws Exception;
    public int deleteLikes(Likes likes)throws Exception;
    public int searchLikes(int fid)throws Exception;	//피드에 좋아요 숫자 알기위한 함수
    public Likes checklikefid(Likes likes)throws Exception;
    
    public boolean checkLikeMe(Likes likes)throws Exception; //내가 좋아요 했는지 안했는지 여부
}
