package com.web.curation.dao.dashboard;


import java.util.List;

import com.web.curation.model.dashboard.Feed;
import com.web.curation.model.dashboard.Feedtag;
import com.web.curation.model.dashboard.Peopletag;


public interface FeedDao {

	public boolean insertFeed(Feed feed)throws Exception;
	
	public boolean updateFeed(Feed feed)throws Exception;
	
	public boolean deleteFeed(int fid)throws Exception;
	
	public List<Feed> searchAllFeed(int uid) throws Exception;		//피드 다 보기
	
	public List<Feed> searchAllmyFeed(int uid) throws Exception;	//내 피드 다 보기

	public List<Feed> searchFeedByLike(int uid)throws Exception;	//좋아요 누른 피드 리스트
	
	public List<Feed> searchFeedByfollow(int uid)throws Exception;	//팔로우 누른 피드 리스트
	
	public List<Feed> searchFeedByHashtag(int uid)throws Exception; //해쉬태그 누른 피드 리스트
	
	public List	<Feed> searchPageFeed(int pageNum)throws Exception;	//스크롤 내릴때마다 순서대로 나오는거 ..헷갈림
	
	public int LikeCountFeed(int fid) throws Exception;	//피드 좋아요 숫자 나오는거

	public int CommentCountFeed(int fid) throws Exception;	//피드 코멘트 숫자 나오는거
	
	public int searchFidBywritedate(Feed feed) throws Exception;
	
	public int insertFeedTag(Feedtag feedtag) throws Exception;

	public int insertPeopleTag(Peopletag peopletag) throws Exception;
	
	public boolean deleteFeedTag(int fid)throws Exception;
	
	public List<Feed> searchFeedByPeopletag(int uid) throws Exception; //내가 태그당한 피드들

	public List<String> searchHashTagByFid(int fid) throws Exception; //피드번호로 해쉬태그

	public List<String> searchPeopleTagByFid(int fid)throws Exception; //피드번호로 사람태그

	public List<Feed> searchFeedByKeyword(String keyword) throws Exception;//스트링 태그로 모든 피드검사
}
