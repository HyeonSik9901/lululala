package com.web.curation.dao.dashboard;

import java.util.List;

import com.web.curation.model.dashboard.CafeRecomment;


public interface CafeRecommentDao {

	public boolean insertCafeRecomment(CafeRecomment caferecomment)throws Exception;

	public boolean updateCafeRecomment(CafeRecomment caferecomment)throws Exception;
	
	public boolean deleteCafeRecomment(int recid)throws Exception;
	
	public List<CafeRecomment> searchCafeRecomment(int cid) throws Exception;

	public int countCafeRecomment (int cid) throws Exception;

}
