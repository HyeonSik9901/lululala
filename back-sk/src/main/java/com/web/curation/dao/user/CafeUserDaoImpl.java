package com.web.curation.dao.user;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.lala.CafeUser;

@Repository
public class CafeUserDaoImpl implements CafeUserDao{

	@Autowired
	SqlSession sqlSession;
	
	private String mapper = "CafeUserMapper.";

	@Override
	public boolean insertCafeUser(CafeUser cafeuser) throws Exception {
		int cnt = sqlSession.insert(mapper+"insertCafeUser",cafeuser);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteCafeUser(int cafeuserid) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteCafeUser",cafeuserid);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<CafeUser> searchCafeMember(int cafeid) throws Exception {
		return sqlSession.selectList(mapper+"searchCafeMember",cafeid);
	}

	@Override
	public boolean updateCafeUser(CafeUser cafeuser) throws Exception {
		int cnt = sqlSession.update(mapper+"updateCafeUser",cafeuser);
		if(cnt!=0) {
			return true;
		}
		return false;
	}



}
