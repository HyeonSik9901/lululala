package com.web.curation.dao.user;

import java.util.List;

import com.web.curation.model.user.Follow;
import com.web.curation.model.user.User;


public interface FollowDao {

	public boolean insertFollow(Follow follow)throws Exception;
	public boolean deleteFollow(Follow follow)throws Exception;
	public List<User> searchFollower(int followeruid)throws Exception;
	public List<User> searchFollowing(int followuid)throws Exception;
	
	//팔로우를 추가하거나, 팔로우를 삭제하거나, 찾는다.
	//2번이 3번을 팔로우한다. 2번이 4번을 팔로우한다. 2--3 , 2--4
}
