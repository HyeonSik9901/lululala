package com.web.curation.dao.dashboard;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.dashboard.Feed;
import com.web.curation.model.dashboard.Feedtag;
import com.web.curation.model.dashboard.Peopletag;

@Repository
public class FeedDaoImpl implements FeedDao{

	private String mapper = "FeedMapper.";
	@Autowired
	private SqlSession sqlSession;
	
	
	@Override
	public boolean insertFeed(Feed feed) throws Exception {
		int cnt = sqlSession.insert(mapper+"insertFeed",feed);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateFeed(Feed feed) throws Exception {
		int cnt = sqlSession.update(mapper+"updateFeed",feed);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteFeed(int fid) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteFeed",fid);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Feed> searchAllFeed(int uid) throws Exception {
		return sqlSession.selectList(mapper+"searchAllFeed",uid);
	}

	@Override
	public List<Feed> searchAllmyFeed(int uid) throws Exception {
		return sqlSession.selectList(mapper+"searchAllmyFeed",uid);
	}

	@Override
	public List<Feed> searchFeedByLike(int uid) throws Exception {
		System.out.println("오나");
		return sqlSession.selectList(mapper+"searchFeedByLike",uid);
	}

	@Override
	public List<Feed> searchFeedByfollow(int uid) throws Exception {
		return sqlSession.selectList(mapper+"searchFeedByfollow",uid);
	}

	@Override
	public List<Feed> searchFeedByHashtag(int uid) throws Exception {
		return sqlSession.selectList(mapper+"searchFeedByHashtag",uid);	
		}

	@Override
	public List<Feed> searchPageFeed(int pageNum) throws Exception {
		return sqlSession.selectList(mapper+"searchPageFeed",pageNum);
	}

	@Override
	public int LikeCountFeed(int fid) throws Exception {
		return sqlSession.selectOne(mapper+"LikeCountFeed",fid);
	}

	@Override
	public int CommentCountFeed(int fid) throws Exception {
		return sqlSession.selectOne(mapper+"CommentCountFeed",fid);
	}

	@Override
	public int searchFidBywritedate(Feed feed) throws Exception {
		return sqlSession.selectOne(mapper+"searchFidBywritedate",feed);
	}

	@Override
	public int insertFeedTag(Feedtag feedtag) throws Exception {
		return sqlSession.insert(mapper+"insertFeedtag",feedtag);
	}

	@Override
	public int insertPeopleTag(Peopletag peopletag) throws Exception {
		return sqlSession.insert(mapper+"insertPeopletag",peopletag);
	}

	@Override
	public boolean deleteFeedTag(int fid) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteFeedTag",fid);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Feed> searchFeedByPeopletag(int uid) throws Exception {
		return sqlSession.selectList(mapper+"searchFeedByPeopletag",uid);
	}

	@Override
	public List<String> searchHashTagByFid(int fid) throws Exception {
		return sqlSession.selectList(mapper+"searchHashTagByFid",fid);
	}

	@Override
	public List<String> searchPeopleTagByFid(int fid) throws Exception {
		return sqlSession.selectList(mapper+"searchPeopleTagByFid",fid);
	}

	@Override
	public List<Feed> searchFeedByKeyword(String keyword) throws Exception {
		return sqlSession.selectList(mapper+"searchFeedByKeyword",keyword);
	}

	

}
