package com.web.curation.dao.dashboard;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.dashboard.Keyword;
import com.web.curation.model.user.Quration;

@Repository
public class KeywordDaoImpl implements KeywordDao{

	private String mapper = "keywordMapper.";
	@Autowired
	private SqlSession sqlSession;
	
	@Override
	public boolean insertKeyword(Quration keyword) throws Exception {
		int cnt = sqlSession.insert(mapper+"insertKeyword",keyword);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteKeyword(int qid) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteKeyword",qid);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Keyword> searchKeyword(int qid) throws Exception {
		return sqlSession.selectList(mapper+"searchKeyword",qid);
	}
	
}
