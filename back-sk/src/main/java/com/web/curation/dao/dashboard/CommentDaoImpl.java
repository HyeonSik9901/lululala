package com.web.curation.dao.dashboard;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.dashboard.Comment;

@Repository
public class CommentDaoImpl implements CommentDao{

	private String mapper = "commentMapper.";
	@Autowired
	private SqlSession sqlSession;
	
	
	@Override
	public boolean insertComment(Comment comment) throws Exception {
		int cnt =sqlSession.insert(mapper+"insertComment",comment);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateComment(Comment comment) throws Exception {
		int cnt = sqlSession.update(mapper+"updateComment",comment);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteComment(int id) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteComment",id);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<Comment> searchComment(int id) throws Exception {
		return sqlSession.selectList(mapper+"searchComment",id);
	}
}
