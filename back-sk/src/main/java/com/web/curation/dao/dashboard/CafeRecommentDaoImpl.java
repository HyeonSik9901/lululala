package com.web.curation.dao.dashboard;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.web.curation.model.dashboard.CafeRecomment;
import com.web.curation.model.dashboard.Recomment;

@Repository
public class CafeRecommentDaoImpl implements CafeRecommentDao{

	@Autowired
	private SqlSession sqlSession;

	private String mapper = "caferecommentMapper.";

	@Override
	public boolean insertCafeRecomment(CafeRecomment caferecomment) throws Exception {
		int cnt =sqlSession.insert(mapper+"insertCafeRecomment",caferecomment);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean updateCafeRecomment(CafeRecomment caferecomment) throws Exception {
		int cnt = sqlSession.update(mapper+"updateCafeRecomment",caferecomment);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteCafeRecomment(int recid) throws Exception {
		int cnt = sqlSession.delete(mapper+"deleteCafeRecomment",recid);
		if(cnt!=0) {
			return true;
		}
		return false;
	}

	@Override
	public List<CafeRecomment> searchCafeRecomment(int cid) throws Exception {
		return sqlSession.selectList(mapper+"searchCafeRecomment",cid);
	}

	@Override
	public int countCafeRecomment(int cid) throws Exception {
		return sqlSession.selectOne(mapper+"countCafeRecomment",cid);
	}
	
	
}
