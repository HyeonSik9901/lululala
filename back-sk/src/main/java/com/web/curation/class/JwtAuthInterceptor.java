// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.servlet.HandlerInterceptor;

// public class JwtAuthInterceptor implements HandlerInterceptor {
//     @Autowired
//     private JwtUtil jwtUtil;
//     @Autowired
//     private UserRepository userRepository;
// ​
//     private String HEADER_TOKEN_KEY = "token";
// ​
//     @Override
//     public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//         User user = userRepository.findById(Long.parseLong(request.getHeader("userId")))
//                 .orElseThrow(() -> new IllegalArgumentException("없는 유저 입니다."));
// ​
//         String givenToken = request.getHeader(HEADER_TOKEN_KEY);
//         verifyToken(givenToken, user.getToken());
// ​
//         return true;
//     }
// ​
//     private void verifyToken(String givenToken, String membersToken) {
//         if(! givenToken.equals(membersToken)){
//             throw new IllegalArgumentException("사용자의 Token과 일치하지 않습니다.");
//         }
// ​
//         jwtUtil.verifyToken(givenToken);
//     }
// }