package com.web.curation.model.user;


import lombok.*;


@ToString
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class AuthenticationRequest {
    
    String email;
    String password;
    String name;
    
}
