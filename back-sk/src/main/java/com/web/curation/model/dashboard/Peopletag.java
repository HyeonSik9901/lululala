package com.web.curation.model.dashboard;

import java.time.LocalDateTime;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Peopletag {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int tid;
	
	 @Column(insertable = false, updatable = false)
	    private LocalDateTime createDate;
	 //태그한 날짜 필요할까?
	 
	 private int fid;
	 
	 private int uid;
	 private String nickname;
	 
	public int getTid() {
		return tid;
	}
	
	public Peopletag(int fid, String nickname) {
		this.fid= fid;
		this.nickname = nickname;
	}
	
	public void setTid(int tid) {
		this.tid = tid;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	@Override
	public String toString() {
		return "Peopletag [createDate=" + createDate + ", fid=" + fid + ", nickname=" + nickname + ", tid=" + tid
				+ ", uid=" + uid + "]";
	}
	
	 
}
