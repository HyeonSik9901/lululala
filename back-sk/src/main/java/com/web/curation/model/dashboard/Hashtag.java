 package com.web.curation.model.dashboard;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Hashtag {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int tid;
	
	 @Column(insertable = false, updatable = false)
	    private LocalDateTime createDate;
	 

	private String tag;
	
	
	public Hashtag(String string) {
		this.tag = string;
	}
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public LocalDateTime getCreateDate() {
		return createDate;
	}
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	@Override
	public String toString() {
		return "Hashtag [tid=" + tid + ", createDate=" + createDate + ", tag=" + tag + "]";
	}
	
}
