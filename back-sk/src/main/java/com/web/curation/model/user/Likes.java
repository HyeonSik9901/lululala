package com.web.curation.model.user;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Likes {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int lid;
	 

	private int fid;
	private int uid;
	

	
	public Likes(int uid, int fid) {
		this.uid=uid;
		this.fid=fid;
	}
	public int getLid() {
		return lid;
	}
	public void setLid(int lid) {
		this.lid = lid;
	}

	public int getFid() {
		return fid;
	}
	public void setFid(int fid) {
		this.fid = fid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	@Override
	public String toString() {
		return "Likes [lid=" + lid + ", fid=" + fid + ", uid=" + uid + "]";
	}
	 
	 
	
}
