package com.web.curation.model.dashboard;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cid;

	@Column(insertable = false, updatable = false)
	private LocalDateTime createDate;
	
	private int fid;
	private int uid;
	private String contents;
	private int count;
	private String profileimg;
	private String nickname;
	
	
	

	public Comment(int cid, int fid, int uid, String contents, LocalDateTime createDate, int count,String profileimg , String nickname) {
		this.cid = cid;
		this.fid = fid;
		this.uid = uid;
		this.contents = contents;
		this.createDate = createDate;
		this.count = count;
		this.profileimg = profileimg;
		this.nickname = nickname;
	}

	
	
	public String getProfileimg() {
		return profileimg;
	}


	public void setProfileimg(String profileimg) {
		this.profileimg = profileimg;
	}


	public String getNickname() {
		return nickname;
	}


	public void setNickname(String nickname) {
		this.nickname = nickname;
	}


	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "Comment [cid=" + cid + ", fid=" + fid + ", uid=" + uid + ", contents=" + contents + ", createDate="
				+ createDate + "]";
	}

	 
}
