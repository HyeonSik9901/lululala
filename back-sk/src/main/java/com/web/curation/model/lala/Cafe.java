package com.web.curation.model.lala;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Cafe {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cafeid;
	
	private int ownerid;
	private String cafename;
	
	private int membercnt;
	
	@Column(insertable = false, updatable = false)
    private LocalDateTime createdate;

	private String intro;
	private int searchcount;
	private String profileimg;
	
	
	
	public String getCafename() {
		return cafename;
	}
	public void setCafename(String cafename) {
		this.cafename = cafename;
	}
	@Override
	public String toString() {
		return "Cafe [cafeid=" + cafeid + ", ownerid=" + ownerid + ", member=" + membercnt + ", createdate=" + createdate
				+ ", intro=" + intro + ", searchcount=" + searchcount + ", profileimg=" + profileimg + "]";
	}
	public int getCafeid() {
		return cafeid;
	}
	public void setCafeid(int cafeid) {
		this.cafeid = cafeid;
	}
	public int getOwnerid() {
		return ownerid;
	}
	public void setOwnerid(int ownerid) {
		this.ownerid = ownerid;
	}
	public int getMember() {
		return membercnt;
	}
	public void setMember(int member) {
		this.membercnt = member;
	}
	public LocalDateTime getCreatedate() {
		return createdate;
	}
	public void setCreatedate(LocalDateTime createdate) {
		this.createdate = createdate;
	}
	public String getIntro() {
		return intro;
	}
	public void setIntro(String intro) {
		this.intro = intro;
	}
	public int getSearchcount() {
		return searchcount;
	}
	public void setSearchcount(int searchcount) {
		this.searchcount = searchcount;
	}
	public String getProfileimg() {
		return profileimg;
	}
	public void setProfileimg(String profileimg) {
		this.profileimg = profileimg;
	}
	
	
	
}
