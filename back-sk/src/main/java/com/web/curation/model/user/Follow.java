package com.web.curation.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Follow {

	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int fid;
    
    private int followuid; // 로그인 한 나 자신
    private int followeruid; // 팔로우 할 대상 또는 피드의 주인
    
    @Column(insertable = false, updatable = false)
    private LocalDateTime createdate;	//팔로우 하게된 날짜 필요 하다해서

	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public int getFollowuid() {
		return followuid;
	}

	public void setFollowuid(int followuid) {
		this.followuid = followuid;
	}

	public int getFolloweruid() {
		return followeruid;
	}

	public void setFolloweruid(int followeruid) {
		this.followeruid = followeruid;
	}

	public LocalDateTime getCreatedate() {
		return createdate;
	}

	public void setCreatedate(LocalDateTime createdate) {
		this.createdate = createdate;
	}

	@Override
	public String toString() {
		return "follow [fid=" + fid + ", followuid=" + followuid + ", followeruid=" + followeruid + ", createdate="
				+ createdate + "]";
	}
    
}
