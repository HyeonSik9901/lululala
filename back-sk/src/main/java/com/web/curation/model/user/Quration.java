package com.web.curation.model.user;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Quration {

	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private int qid;
	 
	@Column(insertable = false, updatable = false)
	private LocalDateTime createdate;
	//큐레이션 등록 날짜 
	  
	private int uid;
	 
	private String name;
	private String tag;
	
	 
	 

	public Quration(int uid, String name) {
		this.uid = uid;
		this.name=name;
	}
	 

	public Quration(int uid, String name , String tag) {
		this.uid = uid;
		this.name=name;
		this.tag=tag;
	}
	 
	public int getQid() {
		return qid;
	}

	public void setQid(int qid) {
		this.qid = qid;
	}

	public LocalDateTime getCreatedate() {
		return createdate;
	}

	public void setCreatedate(LocalDateTime createdate) {
		this.createdate = createdate;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	@Override
	public String toString() {
		return "Quration [createdate=" + createdate + ", name=" + name + ", qid=" + qid + ", tag=" + tag + ", uid="
				+ uid + "]";
	}
	 
	
}
