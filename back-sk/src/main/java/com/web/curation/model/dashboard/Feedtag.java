package com.web.curation.model.dashboard;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Feedtag {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int feedtagid;
	
	private int tid;
	private int fid;
	private String tag;
	
	public Feedtag(String tag, int fid) {
		this.tag=tag;
		this.fid=fid;
	}
	
	
	public int getFeedtagid() {
		return feedtagid;
	}
	public void setFeedtagid(int feedtagid) {
		this.feedtagid = feedtagid;
	}
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public int getFid() {
		return fid;
	}
	public void setFid(int fid) {
		this.fid = fid;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	@Override
	public String toString() {
		return "Feedtag [feedtagid=" + feedtagid + ", fid=" + fid + ", tag=" + tag + ", tid=" + tid + "]";
	}
	
	
}
