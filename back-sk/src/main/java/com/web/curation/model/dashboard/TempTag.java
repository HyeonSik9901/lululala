package com.web.curation.model.dashboard;

public class TempTag {
    int uid;
    String keyword;

    
    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public String toString() {
        return "TempTag [keyword=" + keyword + ", uid=" + uid + "]";
    }
    

}
