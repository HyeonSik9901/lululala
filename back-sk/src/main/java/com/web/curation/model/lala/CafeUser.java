package com.web.curation.model.lala;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class CafeUser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cafeuserid;
	
	private int cafeid;
	private int uid;
	
	private String profileimg;
	private String nickname;
	
	
	public CafeUser(int cafeid, int uid) {
		this.cafeid=cafeid;
		this.uid=uid;
	}
	public CafeUser(int cafeid, int uid,String profileimg, String nickname) {
		this.cafeid=cafeid;
		this.uid=uid;
		this.profileimg = profileimg;
		this.nickname = nickname;
	}
	
	public int getCafeuserid() {
		return cafeuserid;
	}
	public void setCafeuserid(int cafeuserid) {
		this.cafeuserid = cafeuserid;
	}
	public int getCafeid() {
		return cafeid;
	}
	public void setCafeid(int cafeid) {
		this.cafeid = cafeid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public String getProfileimg() {
		return profileimg;
	}
	public void setProfileimg(String profileimg) {
		this.profileimg = profileimg;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	
	
	
}
