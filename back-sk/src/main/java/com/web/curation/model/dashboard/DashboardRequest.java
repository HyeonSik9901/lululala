package com.web.curation.model.dashboard;

import io.swagger.annotations.ApiModelProperty;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;




@Valid
@ToString
public class DashboardRequest {
    
    int number;
    @ApiModelProperty(required = true)
    @NotNull
    String title;
    @ApiModelProperty(required = true)
    @NotNull
    String nickname;
    @ApiModelProperty(required = true)
    @NotNull
    String contents;
    @ApiModelProperty(required = true)
    @NotNull
    String date;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "DashboardRequest [contents=" + contents + ", date=" + date + ", nickname=" + nickname + ", number="
                + number + ", title=" + title + "]";
    }


    
    
}
