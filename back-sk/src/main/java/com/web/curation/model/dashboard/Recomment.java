package com.web.curation.model.dashboard;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Recomment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int recid;

	@Column(insertable = false, updatable = false)
	private LocalDateTime createDate;
	
	private int cid;
	
	private int uid;
	
	private String contents;

	private String profileimg;
	
	private String nickname;
	
	public Recomment(int recid, int cid, int uid, String contents, LocalDateTime localDateTime, String profileimg , String nickname) {
		this.recid = recid;
		this.cid = cid;
		this.uid = uid;
		this.contents = contents;
		this.createDate = localDateTime;
		this.profileimg = profileimg;
		this.nickname = nickname;
	}
	


	public String getProfileimg() {
		return profileimg;
	}

	public void setProfileimg(String profileimg) {
		this.profileimg = profileimg;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getRecid() {
		return recid;
	}

	public void setRecid(int recid) {
		this.recid = recid;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public int getCid() {
		return cid;
	}

	public void setCid(int cid) {
		this.cid = cid;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	@Override
	public String toString() {
		return "Recomment [recid=" + recid + ", createDate=" + createDate + ", cid=" + cid + ", uid=" + uid
				+ ", contents=" + contents + "]";
	}
	
	
	
}
