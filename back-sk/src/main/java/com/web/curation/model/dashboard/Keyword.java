package com.web.curation.model.dashboard;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Keyword {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int kid;
	
	 @Column(insertable = false, updatable = false)
	    private LocalDateTime createDate;
	//키워드 등록날짜?필요할까
	 
	 private int qid;
	 
	 private String tag;
	 
	 
	public int getKid() {
		return kid;
	}

	public void setKid(int kid) {
		this.kid = kid;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}

	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	public int getQid() {
		return qid;
	}

	public void setQid(int qid) {
		this.qid = qid;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	@Override
	public String toString() {
		return "Keyword [kid=" + kid + ", createDate=" + createDate + ", qid=" + qid + ", tag=" + tag + "]";
	}
	 
	 
}
