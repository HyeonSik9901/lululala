package com.web.curation.model;

import org.springframework.web.multipart.MultipartFile;

public class Uploadfile{
    private MultipartFile postImage;

    public MultipartFile getPostImage() {
        return postImage;
    }

    public void setPostImage(MultipartFile postImage) {
        this.postImage = postImage;
    }
    

}