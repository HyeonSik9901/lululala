package com.web.curation.model.dashboard;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Feed {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int fid;
	
	private String contents;
	private int uid;
	private String writedate;
	private String imgurl;
	private String modidate;
	private int open;	//private라는 이름 사용금지
	private String hashtag;
	private String peopletag;
	private String nickname;
	private String profileimg;
	private int likecount;
	private boolean like;
	
	@Column(insertable = false, updatable = false)
	private LocalDateTime createDate;



	// public Feed(int fid, String contents, int uid, String writedate, String imgurl, String modidate, int open,
	// 		String hashtag, String peopletag, String nickname, String profileimg, int likecount, boolean like,
	// 		LocalDateTime createDate) {
	// 	this.fid = fid;
	// 	this.contents = contents;
	// 	this.uid = uid;
	// 	this.writedate = writedate;
	// 	this.imgurl = imgurl;
	// 	this.modidate = modidate;
	// 	this.open = open;
	// 	this.hashtag = hashtag;
	// 	this.peopletag = peopletag;
	// 	this.nickname = nickname;
	// 	this.profileimg = profileimg;
	// 	this.likecount = likecount;
	// 	this.like = like;
	// 	this.createDate = createDate;
	// }	

	public Feed(int uid, String contents, String writedate) {
		this.uid=uid;
		this.contents=contents;
		this.writedate=String.valueOf(LocalDateTime.now());
	}
	
	public int getFid() {
		return fid;
	}

	public void setFid(int fid) {
		this.fid = fid;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getWritedate() {
		return writedate;
	}

	public void setWritedate(String writedate) {
		this.writedate = writedate;
	}

	public String getImgurl() {
		return imgurl;
	}

	public void setImgurl(String imgurl) {
		this.imgurl = imgurl;
	}

	public String getModidate() {
		return modidate;
	}

	public void setModidate(String modidate) {
		this.modidate = modidate;
	}

	public int getOpen() {
		return open;
	}

	public void setOpen(int open) {
		this.open = open;
	}

	public LocalDateTime getCreateDate() {
		return createDate;
	}
	
	public void setCreateDate(LocalDateTime createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "Feed [fid=" + fid + ", contents=" + contents + ", uid=" + uid + ", writedate=" + writedate + ", imgurl="
				+ imgurl + ", modidate=" + modidate + ", open=" + open + ", createDate=" + createDate + "]";
	}

	public String getHashtag() {
		return hashtag;
	}

	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}

	public String getPeopletag() {
		return peopletag;
	}

	public void setPeopletag(String peopletag) {
		this.peopletag = peopletag;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getProfileimg() {
		return profileimg;
	}

	public void setProfileimg(String profileimg) {
		this.profileimg = profileimg;
	}

	public int getLikecount() {
		return likecount;
	}

	public void setLikecount(int likecount) {
		this.likecount = likecount;
	}

	public boolean isLike() {
		return like;
	}

	public void setLike(boolean like) {
		this.like = like;
	}

	 
	
}
