package com.web.curation.controller.dashboard;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.web.curation.model.BasicResponse;
import com.web.curation.model.dashboard.Feed;
import com.web.curation.model.dashboard.Feedtag;
import com.web.curation.model.dashboard.Hashtag;
import com.web.curation.model.dashboard.Peopletag;
import com.web.curation.model.dashboard.Recomment;
import com.web.curation.model.dashboard.TempTag;
import com.web.curation.model.user.Likes;
import com.web.curation.service.dashboard.FeedService;
import com.web.curation.service.dashboard.HashtagService;
import com.web.curation.service.dashboard.LikesService;
import com.web.curation.service.dashboard.PeopletagService;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/feed")
public class FeedController {

   @Autowired
   private FeedService feedService;

   @Autowired
   private HashtagService hashtagService;

   @Autowired
   private PeopletagService peopletagService;
   
   @Autowired
   private LikesService likesService;
   
   @PostMapping("/uploadfile")
   @ApiOperation(value = "사진업로드")
   public Object uploadfile(@RequestPart MultipartFile postImage) throws Exception {
      MultipartFile file = postImage;
      String filename = file.getOriginalFilename();
      String sub=filename;
      char[] arr = filename.toCharArray();
      for(int i=0;i<filename.length();i++){
         if(arr[i] == '.'){
            i++;
            sub = sub.substring(i);
            break;
         }
      }
      BasicResponse result = new BasicResponse();
      UUID uuid = UUID.randomUUID();
      String path = "C:\\Users\\multicampus\\Desktop\\git\\sns-sprint3\\back-sk\\src\\main\\resources\\static\\img\\"+uuid+"."+sub;
      File copyFile = new File(path);
      System.out.println(copyFile.getPath());
      file.transferTo(copyFile);
      path = "http://192.168.100.72:8080/lululala/img/"+uuid+"."+sub;
      System.out.println(path);
      result.status = true;
      result.data = path;
      return new ResponseEntity<>(result, HttpStatus.OK);
   }

   @PostMapping("/insertFeed")
   @ApiOperation(value = "피드등록")
   public Object insertFeed(@RequestBody Feed feed) throws Exception {
         
         String writedate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		 feed.setWritedate(writedate);
		 if(feed.getImgurl().equals("")){
			 feed.setImgurl("http://192.168.100.72:8080/lululala/img/No-Image.png");
		 }
         boolean flag = feedService.insertFeed(feed);
         
         System.out.println(feed.toString());
         String hashtag = feed.getHashtag();
         String peopletag = feed.getPeopletag();
         
         System.out.println("들어온다1번");
         //System.out.println(feed.toString()+"피드 쳐나오나?");
         
         int fid = feedService.searchFidBywritedate(feed);
         
         BasicResponse result = new BasicResponse();
         
         if(hashtag != null){
            StringTokenizer st = new StringTokenizer(hashtag, ",");
            while(st.hasMoreTokens()) {
               String tag = st.nextToken();
               System.out.println(tag);
               boolean hashtagCheck = hashtagService.searchHashtagByCheck(tag);
               if(hashtagCheck) {   //이게 참이면 디비에 처음 등록된 해쉬태그라는 뜻입니다.
                  Hashtag hashtag2 = new Hashtag(tag);
                  System.out.println("들어온다2번"+" "+hashtag2);
                  System.out.println(hashtag2);
                  boolean insertFlag = hashtagService.insertHashtag(tag);
                  System.out.println(insertFlag);
                  System.out.println(writedate);
                  System.out.println(feed.getUid()+"uid");
                  System.out.println(feed.getContents()+"contents");
                  
                  System.out.println(fid);
                  Feedtag feedtag = new Feedtag(tag,fid);
                  int cnt = feedService.insertFeedTag(feedtag);
                  if(cnt!=0) {
                     System.out.println("피드 해쉬태그 추가 성공");
                  }else {
                     System.out.println("피드 해쉬태그 추가 실패");
                  }
               }else {   //false 면 이미 디비에 등록되어있는 해쉬태그 
                  
                  Feedtag feedtag = new Feedtag(tag,fid);   //피드태그 만들고
                  int cnt = feedService.insertFeedTag(feedtag);   //피드태그 추가
                  if(cnt!=0) {
                     System.out.println("피드 해쉬 태그 추가 성공");
                  }else {
                     System.out.println("피드 해쉬 태그 추가 실패");
                  }
               }
            }
         }
         System.out.println(peopletag);
         if(peopletag != null) {
            StringTokenizer stBypeople = new StringTokenizer(peopletag, ",");
            System.out.println(stBypeople);
            while(stBypeople.hasMoreTokens()) {
               String people = stBypeople.nextToken();
               boolean peopletagCheck = peopletagService.searchPeopleByCheck(people);
               if(peopletagCheck) {   //닉네임으로 사람 검색완료
                  Peopletag nickname = new Peopletag(fid,people);
                  int cnt = feedService.insertPeopletag(nickname);   //피플태그 등록
                  if(cnt!=0) {
                     System.out.println("피드에 사람태그 추가 성공");
                  }else {
                     System.out.println("피드에 사람 태그 추가 실패");
                  }
               }else {   //없는 닉네임 태그 
                  System.out.println("없는 닉네임들어왔음");
               }
            }
         }
         //해쉬태그 등록 완료 
      if (!flag) {
         System.out.println("삽입오류");
         result.status = false;
         result.data = "fail";
      } else {
         result.status = true;
         result.data = "succ";
      }
      return new ResponseEntity<>(result, HttpStatus.OK);
   }
   
   
   @PostMapping("/searchAllFeed/{uid}") // 피드 목록 띄우기
   @ApiOperation(value = "전체피드")
   public Object searchAllFeed(@PathVariable int uid) throws Exception {
      System.out.println(uid);
      List<Feed> list = feedService.searchAllFeed(uid);
                                       
      List<Feed> temp = new ArrayList<Feed>();   //피드에 좋아요 수랑 좋아요 여부를 넣기 위한 로직
      for(int i=0; i < list.size(); i++) {
         Feed tempFeed = list.get(i);
         int likeCount = likesService.searchLikes(tempFeed.getFid());//피드에 좋아요 숫자담을꺼
         boolean likeFlag = likesService.checkLikeMe(new Likes(uid,tempFeed.getFid()));//내가 좋아요 했는지 안했는지 여부
         
         Feed likeFeed = new Feed(tempFeed.getFid(),tempFeed.getContents(),tempFeed.getUid(),tempFeed.getWritedate()
               ,tempFeed.getImgurl(),tempFeed.getModidate(),tempFeed.getOpen(),tempFeed.getHashtag(),tempFeed.getPeopletag(),
               tempFeed.getNickname(),tempFeed.getProfileimg(),likeCount,likeFlag,tempFeed.getCreateDate());
         temp.add(likeFeed);
      }
      BasicResponse result = new BasicResponse();
      JSONObject dummy = new JSONObject();
      dummy.put("feedlist", temp);
      if (temp.isEmpty()) {
         System.out.println("피드가 아에 없음");
         result.status = false;
         result.data = "fail";
         return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
      } else {
         System.out.println("피드가있음");
         result.status = true;
         result.data = "succ";
      }
      result.object = dummy.toMap();
      return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
   }

   @PostMapping("/searchAllmyFeed/{uid}") //내 피드 목록 띄우기
   @ApiOperation(value = "내전체피드")
   public Object searchAllmyFeed(@PathVariable int uid) throws Exception {
      System.out.println(uid+"번 내 피드보기");
      List<Feed> list = feedService.searchAllmyFeed(uid);
      BasicResponse result = new BasicResponse();
      
      JSONObject dummy = new JSONObject();
      dummy.put("feedlist", list);
      if (list.isEmpty()) {
         System.out.println("피드가 아에 없음");
         result.status = false;
         result.data = "fail";
         return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
      } else {
         System.out.println("피드가있음");
         result.status = true;
         result.data = "succ";
      }
      result.object = dummy.toMap();
      return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
   }
   
   @PutMapping("/updateFeed")
   @ApiOperation(value= "피드수정")
   public ResponseEntity<BasicResponse> updateFeed(@RequestBody Feed feed, @RequestParam(required = false) String hashtag, @RequestParam(required = false) String peopletag)throws Exception{
      boolean flag = feedService.updateFeed(feed);
      //피드 내용물 일단 다 업데이트 하고
      //사람 태그와 피드에 등록되어있는 해쉬 태그들 다 삭제하고 다시 삽입한다.
      boolean feedTagByDelete = feedService.deleteFeedTag(feed.getFid());
      
      if(!feedTagByDelete) {
         System.out.println("피드태크 삭제실패");
      }
      boolean peopleTagByDelete = peopletagService.deletePeopletagByFeed(feed.getFid());
      if(!peopleTagByDelete) {
         System.out.println("사람태그 삭제 실패");
      }
      
      if(hashtag != null){
         StringTokenizer st = new StringTokenizer(hashtag, ",");
         while(st.hasMoreTokens()) {
            String tag = st.nextToken();
            System.out.println(tag);
            boolean hashtagCheck = hashtagService.searchHashtagByCheck(tag);
            if(hashtagCheck) {   //이게 참이면 디비에 처음 등록된 해쉬태그라는 뜻입니다.
               Hashtag hashtag2 = new Hashtag(tag);
               System.out.println("들어온다2번"+" "+hashtag2);
               System.out.println(hashtag2);
               boolean insertFlag = hashtagService.insertHashtag(tag);
               System.out.println(insertFlag);
               System.out.println(feed.getUid()+"uid");
               System.out.println(feed.getContents()+"contents");
               
               Feedtag feedtag = new Feedtag(tag,feed.getFid());
               int cnt = feedService.insertFeedTag(feedtag);
               if(cnt!=0) {
                  System.out.println("피드 해쉬태그 추가 성공");
               }else {
                  System.out.println("피드 해쉬태그 추가 실패");
               }
            }else {   //false 면 이미 디비에 등록되어있는 해쉬태그 
               
               Feedtag feedtag = new Feedtag(tag,feed.getFid());   //피드태그 만들고
               int cnt = feedService.insertFeedTag(feedtag);   //피드태그 추가
               if(cnt!=0) {
                  System.out.println("피드 해쉬 태그 추가 성공");
               }else {
                  System.out.println("피드 해쉬 태그 추가 실패");
               }
            }
         }
         
      }
      System.out.println(peopletag);
      if(peopletag != null) {
         StringTokenizer stBypeople = new StringTokenizer(peopletag, ",");
         System.out.println(stBypeople);
         while(stBypeople.hasMoreTokens()) {
            String people = stBypeople.nextToken();
            boolean peopletagCheck = peopletagService.searchPeopleByCheck(people);
            if(peopletagCheck) {   //닉네임으로 사람 검색완료
               Peopletag nickname = new Peopletag(feed.getFid(),people);
               int cnt = feedService.insertPeopletag(nickname);   //피플태그 등록
               if(cnt!=0) {
                  System.out.println("피드에 사람태그 추가 성공");
               }else {
                  System.out.println("피드에 사람 태그 추가 실패");
               }
            }else {   //없는 닉네임 태그 
               System.out.println("없는 닉네임들어왔음");
            }
         }
      }
      BasicResponse result = new BasicResponse();
      if(!flag) {
         System.out.println("피드수정실패");
         result.status = false;
         result.data="피드없거나 오류";
      }else {
         result.status = true;
         result.data = "succ";
      }
      
      return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
   }
   
       @DeleteMapping("/deleteFeed/{fid}")
       @ApiOperation(value = "피드삭제")
      public ResponseEntity<BasicResponse> deleteComment(@PathVariable int fid) throws Exception{
         boolean flag = feedService.deleteFeed(fid);
         BasicResponse result = new BasicResponse();
         if(!flag) {
         System.out.println("삭제오류");
         result.status = false;
         result.data = "fail";
         }else {
            result.status = true;
            result.data ="succ";
         }
         return new ResponseEntity<BasicResponse>(result,HttpStatus.OK);
      }
       
       @PostMapping("/searchFeedByLike/{uid}") // 좋아요 한 피드 보기
      @ApiOperation(value = "좋아요피드")
      public Object searchFeedByLike(@PathVariable int uid) throws Exception {
         System.out.println("여긴옴");
          List<Feed> list = feedService.searchFeedByLike(uid);
         BasicResponse result = new BasicResponse();
         System.out.println(list.toString());
         JSONObject dummy = new JSONObject();
         dummy.put("feedlist", list);
         if (list.isEmpty()) {
            System.out.println("피드가 아에 없음");
            result.status = false;
            result.data = "fail";
            return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
         } else {
            System.out.println("피드가있음");
            result.status = true;
            result.data = "succ";
         }
         result.object = dummy.toMap();
         return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
      }
       
       @GetMapping("/LikeCountFeed/{fid}") // 피드에 좋아요 숫자
      @ApiOperation(value = "좋아요수")
      public Object LikeCountFeed(@PathVariable int fid) throws Exception {
         int likeCount = feedService.LikeCountFeed(fid);
         BasicResponse result = new BasicResponse();
         //System.out.println("피드가있음");
         result.status = true;
         result.data = "succ";
         JSONObject dummy = new JSONObject();
         dummy.put("likeCount", likeCount);
         result.object = dummy.toMap();
         //그냥 BasicResponse말고 likeCount를 인트형으로 쏴도될듯
         return new ResponseEntity<>(result, HttpStatus.OK);
      }
       
       @GetMapping("/CommentCountFeed/{fid}") // 피드에 댓글수
      @ApiOperation(value = "댓글수")
      public Object CommentCountFeed(@PathVariable int fid) throws Exception {
         int commentCount = feedService.CommentCountFeed(fid);
         BasicResponse result = new BasicResponse();
         //System.out.println("피드가있음");
         result.status = true;
         result.data = "succ";
         JSONObject dummy = new JSONObject();
         dummy.put("commentCount", commentCount);
         result.object = dummy.toMap();
         //그냥 BasicResponse말고 likeCount를 인트형으로 쏴도될듯
         return new ResponseEntity<>(result, HttpStatus.OK);
      }
       
       @PostMapping("/searchFeedByPeopletag/{uid}") // 피플태그로 내 태그한 글들보기
      @ApiOperation(value = "나를태그한피드")
      public Object searchFeedByPeopletag(@PathVariable int uid) throws Exception {
         System.out.println("피플태그");
          List<Feed> list = feedService.searchFeedByPeopletag(uid);
         BasicResponse result = new BasicResponse();
         System.out.println(list.toString());
         JSONObject dummy = new JSONObject();
         dummy.put("feedlist", list);
         if (list.isEmpty()) {
            System.out.println("피드가 아에 없음");
            result.status = false;
            result.data = "fail";
            return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
         } else {
            System.out.println("피드가있음");
            result.status = true;
            result.data = "succ";
         }
         result.object = dummy.toMap();
         return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
      }
       
       @PostMapping("/searchFeedByHashtag/{uid}") // 해쉬태그한 피드 보기
      @ApiOperation(value = "해쉬태그 피드")
      public Object searchFeedByHashtag(@PathVariable int uid) throws Exception {
         List<Feed> list = feedService.searchFeedByHashtag(uid);
         BasicResponse result = new BasicResponse();
         System.out.println("진짜필요한순간이다!!!!!!!!!!");
         System.out.println("dd"+list);
         JSONObject dummy = new JSONObject();
         dummy.put("feedlist", list);
         if (list.isEmpty()) {
            System.out.println("피드가 아에 없음");
            result.status = false;
            result.data = "fail";
            return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);

         } else {
            System.out.println("피드가있음");
            result.status = true;
            result.data = "succ";
         }
         result.object = dummy.toMap();
         return new ResponseEntity<>(result, HttpStatus.OK);
      }
       
       
       @PostMapping("/searchTagByFid/{fid}") // 피드번호를 기준으로 해쉬태그 피플태그 가져오기
      @ApiOperation(value = "태그들보기")
      public Object searchTagByFid(@PathVariable int fid) throws Exception {

          List<String> hashtag = feedService.searchHashTagByFid(fid);
          List<String> peopletag = feedService.searchPeopleTagByFid(fid);
          BasicResponse result = new BasicResponse();
         JSONObject dummy = new JSONObject();
         dummy.put("hashtag", hashtag);
         dummy.put("peopletag", peopletag);
         if (hashtag.isEmpty()) {
            System.out.println("해쉬태그 없음");
            result.status = false;
            result.data = "fail";
            return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
         } else {
            System.out.println("태그가있음");
            result.status = true;
            result.data = "succ";
         }
         result.object = dummy.toMap();
         return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
      }
       
       
       @PostMapping("/searchFeedByKeyword") // 태그검색으로  목록 띄우기
      @ApiOperation(value = "keyword로피드 검색")
      public Object searchFeedByKeyword(@RequestBody TempTag temptag) throws Exception {
         //tempTag는 keyword와 uid로 이루어져있음
          
          System.out.println(temptag.getKeyword());   
          int uid = temptag.getUid();
         List<Feed> list = feedService.searchFeedByKeyword(temptag.getKeyword());
         
         List<Feed> temp = new ArrayList<Feed>();   //피드에 좋아요 수랑 좋아요 여부를 넣기 위한 로직
         for(int i=0; i < list.size(); i++) {
            Feed tempFeed = list.get(i);
            if(tempFeed.getOpen()!=0) {
               continue;
            }
            
            int likeCount = likesService.searchLikes(tempFeed.getFid());//피드에 좋아요 숫자담을꺼
            boolean likeFlag = likesService.checkLikeMe(new Likes(uid,tempFeed.getFid()));//내가 좋아요 했는지 안했는지 여부
            
            Feed likeFeed = new Feed(tempFeed.getFid(),tempFeed.getContents(),tempFeed.getUid(),tempFeed.getWritedate()
                  ,tempFeed.getImgurl(),tempFeed.getModidate(),tempFeed.getOpen(),tempFeed.getHashtag(),tempFeed.getPeopletag(),
                  tempFeed.getNickname(),tempFeed.getProfileimg(),likeCount,likeFlag,tempFeed.getCreateDate());
            temp.add(likeFeed);
         }
         BasicResponse result = new BasicResponse();
         JSONObject dummy = new JSONObject();
         dummy.put("feedlist", temp);
         if (temp.isEmpty()) {
            System.out.println("피드가 아에 없음");
            result.status = false;
            result.data = "fail";
            return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
         } else {
            System.out.println("피드가있음");
            result.status = true;
            result.data = "succ";
         }
         result.object = dummy.toMap();
         return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
      }
       
}