package com.web.curation.controller.account;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.web.curation.model.BasicResponse;
import com.web.curation.model.lala.Cafe;
import com.web.curation.model.lala.CafeUser;
import com.web.curation.model.user.User;
import com.web.curation.service.user.CafeUserDaoService;
import com.web.curation.service.user.UserDaoService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ApiResponses(value = { @ApiResponse(code = 401, message = "Unauthorized", response = BasicResponse.class),
        @ApiResponse(code = 403, message = "Forbidden", response = BasicResponse.class),
        @ApiResponse(code = 404, message = "Not Found", response = BasicResponse.class),
        @ApiResponse(code = 500, message = "Failure", response = BasicResponse.class) })

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/cafeuser")
public class CafeUserController {

	@Autowired
	CafeUserDaoService cafeuserdaoservice;
	
	@Autowired
	UserDaoService userdaoservice;
	
	@PostMapping("/insertCafeUser")
	@ApiOperation(value = "카페가입하기")
	public ResponseEntity<BasicResponse> insertCafeUser(@RequestBody CafeUser cafeuser)
			throws Exception {
		
		//카페 아이디와 유저아이디만 일단 받아온다.
		User user  = userdaoservice.findUserByUid(cafeuser.getUid());
		CafeUser cafeMember = new CafeUser(cafeuser.getCafeid(),cafeuser.getUid(),user.getProfileimg(),user.getNickname());
		
		System.out.println("카페추가");
		boolean flag = cafeuserdaoservice.insertCafeUser(cafeMember);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("삽입오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	@DeleteMapping("/deleteCafeUser/{cafeuserid}")
	@ApiOperation(value = "회원탈퇴")
	public ResponseEntity<BasicResponse> deleteCafeUser(@PathVariable int cafeuserid) throws Exception {
		boolean flag = cafeuserdaoservice.deleteCafeUser(cafeuserid);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("탈퇴오류 ");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}

		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	//프로필 이미지 , 닉네임 수정
	@PutMapping("/updateCafeUser")
	@ApiOperation(value = "회원수정")
	public ResponseEntity<BasicResponse> updateCafeUser(@RequestBody CafeUser cafeuser) throws Exception {
		boolean flag = cafeuserdaoservice.updateCafeUser(cafeuser);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("카페수정");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	@PostMapping("/searchCafeMember/{cafeid}") //카페 회원목록 띄우기
	@ApiOperation(value = "카페회원목록")
	public Object searchCafeMember(@PathVariable int cafeid) throws Exception {
		List<CafeUser> list = cafeuserdaoservice.searchCafeMember(cafeid);
		BasicResponse result = new BasicResponse();
		
		JSONObject dummy = new JSONObject();
		dummy.put("cafeMemberlist", list);
		if (list.isEmpty()) {
			System.out.println("회원이없을순없으니오류");
			result.status = false;
			result.data = "fail";
			return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
		} else {
			System.out.println("회원목록정상호출");
			result.status = true;
			result.data = "succ";
		}
		result.object = dummy.toMap();
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	
}
