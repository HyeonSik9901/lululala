package com.web.curation.controller.account;

import com.web.curation.dao.dashboard.LikesDao;
import com.web.curation.model.BasicResponse;
import com.web.curation.model.user.Follow;
import com.web.curation.model.user.Likes;
import com.web.curation.model.user.RefreshKey;
import com.web.curation.model.user.User;
import com.web.curation.service.user.FollowDaoService;
import com.web.curation.service.user.RefreshKeyService;
import com.web.curation.service.user.UserDaoService;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;

import java.io.File;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.json.JSONObject;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.swagger.annotations.ApiOperation;

@ApiResponses(value = { @ApiResponse(code = 401, message = "Unauthorized", response = BasicResponse.class),
        @ApiResponse(code = 403, message = "Forbidden", response = BasicResponse.class),
        @ApiResponse(code = 404, message = "Not Found", response = BasicResponse.class),
        @ApiResponse(code = 500, message = "Failure", response = BasicResponse.class) })

@RestController
@CrossOrigin(origins = "*")
public class AccountController {
    // 디비 셋팅 후 주석을 푸세요.
    @Autowired
    UserDaoService service;
    @Autowired
    FollowDaoService followservice;

    @Autowired
    JavaMailSender javaMailSender;

    @Autowired
    RefreshKeyService refreshKeyService;

    @Autowired
    FollowDaoService followDaoService;

    @Autowired
    LikesDao likesDao;

    // 토큰 생성
    public static String createToken(User user, String token) {
        String key = "lululala";

        Map<String, Object> headers = new HashMap<>();
        headers.put("typ", "JWT");
        headers.put("alg", "HS256");

        Map<String, Object> payloads = new HashMap<>();
        Long expiredTime = 180 * 1000 * 60l; // 만료기간 180분
        if (token.equals("refresh")) {
            expiredTime = 14 * 24 * 1000 * 60l;
        }
        Date now = new Date();
        now.setTime(now.getTime() + expiredTime);
        payloads.put("exp", now);
        payloads.put("uid", user.getUid());
        payloads.put("email", user.getEmail());
        payloads.put("nickname", user.getNickname());
        payloads.put("intro", user.getIntro());
        payloads.put("profileimg", user.getProfileimg());

        String jwt = Jwts.builder().setHeader(headers).setClaims(payloads)
                .signWith(SignatureAlgorithm.HS256, key.getBytes()).compact();
        return jwt;
    }

    // 토큰 해독
    public static int getTokenFromJwtString(String jwtTokenString) throws InterruptedException {
        Claims claims = Jwts.parser().setSigningKey("lululala".getBytes()).parseClaimsJws(jwtTokenString).getBody();

        // Date expiration = claims.get("exp", Date.class);
        // System.out.println(expiration+"만료 시간");

        // String data = claims.get("data", String.class);
        // System.out.println(data);
        System.out.println(claims.get("uid", Integer.class));
        // user.setUid(Integer.parseInt(claims.get("uid", String.class)));
        return claims.get("uid", Integer.class);
    }
    public static boolean getTokendate(String jwtTokenString) throws InterruptedException {
        Claims claims = Jwts.parser().setSigningKey("lululala".getBytes()).parseClaimsJws(jwtTokenString).getBody();
        Date expiration = claims.get("exp", Date.class);
        System.out.println(expiration+"만료 시간");

        return true;
    }

    // 그냥 텍스트 메일
    public MimeMessage sendMail(String address, String title, String contents) throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        message.setSubject(title);
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(address));
        message.setText(contents);
        message.setSentDate(new Date());
        javaMailSender.send(message);
        return message;
    }
    @PostMapping("/account/uploadfile")
	@ApiOperation(value = "사진업로드")
	public Object uploadfile(@RequestPart MultipartFile postImage) throws Exception {
		MultipartFile file = postImage;
		String filename = file.getOriginalFilename();
		String sub=filename;
		char[] arr = filename.toCharArray();
		for(int i=0;i<filename.length();i++){
			if(arr[i] == '.'){
				i++;
				sub = sub.substring(i);
				break;
			}
		}
		

		
		BasicResponse result = new BasicResponse();
		
		UUID uuid = UUID.randomUUID();
		String path = "C:\\Users\\multicampus\\Desktop\\git\\sns-sprint3\\back-sk\\src\\main\\resources\\static\\profileimg\\"+uuid+"."+sub;
		File copyFile = new File(path);
		
		System.out.println(copyFile.getPath());

		file.transferTo(copyFile);
		path = "http://localhost:8080/lululala/profileimg/"+uuid+"."+sub;
		System.out.println(path);
		result.status = true;
		result.data = path;
		
		return new ResponseEntity<>(result, HttpStatus.OK);

	}

    @PostMapping("/account/mailsend")
    @ApiOperation(value = "회원가입 메일 확인")
    public Object mailsend(@RequestParam(required = true) final String email) throws SQLException, MessagingException {

        JSONObject dummyData = new JSONObject();
        final BasicResponse result = new BasicResponse();
        int rand = (int) (Math.random() * 999999) + 100000;
        System.out.println(email);

        User user = service.findUserByEmail(email);
        result.data = "succ";

        if (user == null) {
            sendMail(email, "룰루랄라 이메일 인증입니다", rand + "");
            dummyData.put("random", rand);
        } else {
            result.data = "중복";
        }

        result.status = true;

        result.object = dummyData.toMap();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/account/findpasswordmailsend")
    @ApiOperation(value = "회원가입 메일 확인")
    public Object findpasswordmailsend(@RequestParam(required = true) final String email) throws SQLException, MessagingException {

        JSONObject dummyData = new JSONObject();
        final BasicResponse result = new BasicResponse();


        User user = service.findUserByEmail(email);
        result.data = "succ";
        System.out.println(user.toString());
        if (user != null) {
            sendMail(email, "룰루랄라 이메일 인증입니다", user.getPassword());

        } else {
            result.data = "fail";
        }

        result.status = true;

        result.object = dummyData.toMap();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/account/tokenuser/{token}")
    @ApiOperation(value = "토큰을 받아 유저 정보를 리턴")
    public Object tokenreturnuser(@PathVariable String token) {
        System.out.println("받는 토큰");
        System.out.println(token);
        JSONObject dummyUser = new JSONObject();
        int uid = 0;
        try {
            uid = getTokenFromJwtString(token);
            System.out.println(uid+"번의 토큰을 해독함");
        } catch (InterruptedException e) {
            System.out.println("왜또안돼");
        }
        User user = new User();
        try {
            user = service.searchUser(uid);
        } catch (SQLException e) {
            
        }
        final BasicResponse result = new BasicResponse();
        if(uid == 0){
            result.status = true;
            result.data = "fail";
            result.object = dummyUser.toMap();
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        dummyUser.put("uid", user.getUid());
        dummyUser.put("email", user.getEmail());
        dummyUser.put("intro", user.getIntro());
        dummyUser.put("nickname", user.getNickname());
        dummyUser.put("profileimg", user.getProfileimg());
        dummyUser.put("usertitle", user.getUsertitle());
        dummyUser.put("createdate", user.getcreatedate());


        

        
        
       
        result.status = true;

        result.object = dummyUser.toMap();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    @PostMapping("/account/kakaologin")
    @ApiOperation(value = "카카오로그인")
    public Object kakaologin(@RequestParam(required = true) final String email) throws Exception{

        JSONObject dummyUser = new JSONObject();
        System.out.println("카카오 로그인");
        System.out.println(email);
        User user = new User();
        final BasicResponse result = new BasicResponse();
        user = service.findUserByEmail(email);
        if(user != null)
        {

            System.out.println(user.toString());

            String atoken = "";
            String rtoken = "암것도 음슴";
            if (user.getUid() != 0) {
                atoken = createToken(user, "access");

                try {
                    rtoken = refreshKeyService.searchuid(user.getUid());

                    if (rtoken == null) {
                        System.out.println("키없슴");
                        rtoken = createToken(user, "refresh");
                        RefreshKey refreshKey = new RefreshKey();
                        refreshKey.setUid(user.getUid());
                        refreshKey.setValue(rtoken);
                        try {
                            refreshKeyService.insertRefreshKey(refreshKey);
                            System.out.println("키드감");
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    } else {
                        System.out.println("키있슴");
                    }
                } catch (Exception e) {

                }

                result.token = atoken;
                result.refresh_token = rtoken;
                result.data = "succ";
            }
            try {
                getTokenFromJwtString(atoken);
                getTokendate(rtoken);
            } catch (InterruptedException e) {
                System.out.println("왜또안돼");
            }
            result.status = true;
        }
        else{
            result.status = true;
            result.data = "fail";
        }

        result.object = dummyUser.toMap();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/account/login")
    @ApiOperation(value = "로그인")
    public Object login(@RequestParam(required = true) final String email,
            @RequestParam(required = true) final String password) {

        JSONObject dummyUser = new JSONObject();

        User user = new User();
        user.setEmail(email);
        user.setPassword(password);

        user = service.login(user);

        final BasicResponse result = new BasicResponse();

        System.out.println(user.toString());

        result.data = "fail";
        String atoken = "";
        String rtoken = "암것도 음슴";
        if (user.getUid() != 0) {
            atoken = createToken(user, "access");

            try {
                rtoken = refreshKeyService.searchuid(user.getUid());

                if (rtoken == null) {
                    System.out.println("키없슴");
                    rtoken = createToken(user, "refresh");
                    RefreshKey refreshKey = new RefreshKey();
                    refreshKey.setUid(user.getUid());
                    refreshKey.setValue(rtoken);
                    try {
                        refreshKeyService.insertRefreshKey(refreshKey);
                        System.out.println("키드감");
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                } else {
                    System.out.println("키있슴");
                }
            } catch (Exception e) {

            }

            result.token = atoken;
            result.refresh_token = rtoken;
            result.data = "succ";
        }
        try {
            getTokenFromJwtString(atoken);
            getTokendate(rtoken);
        } catch (InterruptedException e) {
            System.out.println("왜또안돼");
        }
        result.status = true;

        result.object = dummyUser.toMap();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/account/refresh")
    @ApiOperation(value = "refresh 토큰")
    public Object refresh(@RequestParam(required = true) final String refresh_token) {
        RefreshKey refreshKey = new RefreshKey();
        refreshKey.setValue(refresh_token);
        boolean flag = false;
        String db_refresh_token = "";
        try {
            db_refresh_token = refreshKeyService.searchRefreshKey(refreshKey);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (db_refresh_token.equals(refresh_token)) {
            flag = true;
        }
        JSONObject dummyUser = new JSONObject();

        final BasicResponse result = new BasicResponse();
        if (flag) {
            int id=0;
            User user = new User();
            try {
                id = getTokenFromJwtString(refresh_token);
                user = service.searchUser(id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            String token = createToken(user, "access");
            result.status = true;
            result.data = "success";
            result.token = token;
            result.object = dummyUser.toMap();
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        else{
            result.status = true;
            result.data = "fail";
            result.object = dummyUser.toMap();
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
       
    }
    
    @PostMapping("/account/logout")
    @ApiOperation(value = "로그아웃")
    public Object logout(@RequestParam(required = true) final String email,
                        @RequestParam(required = true) final String password){
        JSONObject dummyUser = new JSONObject();
        System.out.println(email);
        System.out.println(password);
        final BasicResponse result = new BasicResponse();
        result.status = true;
        result.data = "succ";
        result.object = dummyUser.toMap();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/account/signup")
    @ApiOperation(value = "가입하기")
    public Object signup(@RequestBody User user) throws Exception {
        //이메일, 닉네임 중복처리 필수
        //회원가입단을 생성해 보세요.
        
        user.setProfileimg("http://localhost:8080/lululala/profileimg/No-Image.png");

        System.out.println(user.toString());
        int cnt = service.addUser(user);
    	final BasicResponse result = new BasicResponse();
        result.status = true;
        result.data = "succ";
        if( cnt == 0){
            result.data = "fail";
        }else{
           User us = service.findUserByEmail(user.getEmail());
           Follow follow = new Follow();
           follow.setFolloweruid(1);
           follow.setFollowuid(us.getUid());
           followDaoService.insertFollow(follow);
           Likes likes = new Likes();
           likes.setUid(us.getUid());
           likes.setFid(1);
           likesDao.insertLikes(likes);
        }
        System.out.println(result.data);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    
    @DeleteMapping("/account/delete")
    @ApiOperation(value = "삭제하기")
    public Object delete(@RequestBody User user) throws SQLException {
    	
    	boolean flag = service.deleteUser(user.getEmail());
        final BasicResponse result = new BasicResponse();
        if(flag) {
        result.status = true;
        result.data = "succ";
        }else {
        	result.status = false;
            result.data = "fail";
        }
        System.out.println(result.data);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @PutMapping("/account/update")
    @ApiOperation(value="업데이트")
    public Object update(@RequestBody User user) throws SQLException {
        
		boolean flag = service.updateUser(user);
		final BasicResponse result = new BasicResponse();
        if(flag) {
        result.status = true;
        result.data = "succ";
        }else {
        	result.status = false;
            result.data = "fail";
        }
        System.out.println(result.data);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    @PostMapping("/account/search")
    @ApiOperation(value = "모든유저검색하기")
    public Object search(@RequestBody User user) throws SQLException {
        List<User> list = service.searchNick(user.getNickname());
        final BasicResponse result = new BasicResponse();
        result.status = true;
        result.data = "succ";
        JSONObject dummyUser = new JSONObject();
        dummyUser.put("userlist", list);
        result.object = dummyUser.toMap();
        System.out.println(result.data);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @PostMapping("/account/insertfollow")
    @ApiOperation(value = "팔로우추가")
    public Object insertFollow(@RequestBody Follow follow) throws Exception {
        //이메일, 닉네임 중복처리 필수
        //회원가입단을 생성해 보세요.
        
    	System.out.println(follow.toString());
    	boolean cnt = followservice.insertFollow(follow);
    	final BasicResponse result = new BasicResponse();
        if(!cnt){
            result.data = "fail";
            return new ResponseEntity<>(result, HttpStatus.NO_CONTENT);
        }
        result.status = true;
        result.data = "succ";
        System.out.println(result.data);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @PostMapping("/account/deletefollow")
    @ApiOperation(value = "팔로워삭제하기")
    public Object deleteFollow(@RequestBody Follow follow) throws Exception {
        //이메일, 닉네임 중복처리 필수
        //회원가입단을 생성해 보세요.
        boolean flag = followservice.deleteFollow(follow);
        final BasicResponse result = new BasicResponse();
        if(flag) {
        result.status = true;
        result.data = "succ";
        }else {
        	result.status = false;
            result.data = "fail";
        }
        System.out.println(result.data);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @PostMapping("/account/searchFollower/{nickname}")
    @ApiOperation(value = "팔로우목록")
    public Object searchFollower(@PathVariable String nickname) throws NumberFormatException, Exception {
        User user = service.findUserByNick(nickname);
        int followuid = user.getUid();
        List<User> list = followservice.searchFollower(followuid);
        final BasicResponse result = new BasicResponse();
        
        result.status = true;
        result.data = "succ";
        
        JSONObject dummyUser = new JSONObject();
        System.out.println(list.toString());
        dummyUser.put("userlist", list);
        
        result.object = dummyUser.toMap();
        System.out.println(result.data);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @PostMapping("/account/searchFollowing/{nickname}")
    @ApiOperation(value = "팔로잉목록")
    public Object searchFollowing(@PathVariable String nickname) throws NumberFormatException, Exception {
        User user = service.findUserByNick(nickname);
        int followuid = user.getUid();
    	List<User> list = followservice.searchFollowing(followuid);
        final BasicResponse result = new BasicResponse();
        
        result.status = true;
        result.data = "succ";
        
        JSONObject dummyUser = new JSONObject();
        System.out.println(list.toString());
        dummyUser.put("userlist", list);
        
        result.object = dummyUser.toMap();
        System.out.println(result.data);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    @PostMapping("/account/searchNickname/{nickname}")
    @ApiOperation(value = "닉네임중복확인")
    public Object searchNickname(@PathVariable String nickname)throws Exception{
    	boolean flag = service.searchNickname(nickname);
    	final BasicResponse result = new BasicResponse();
    	JSONObject dummyUser = new JSONObject();
    	if(flag) {
            
            result.status = true;
            result.data = "succ";
            result.object = dummyUser.toMap();
            return new ResponseEntity<>(result,HttpStatus.OK);
    	}
    	return new ResponseEntity<>(result,HttpStatus.OK);
    }
    
    @PostMapping("/account/findUserByEmail/{email}")
    @ApiOperation(value = "email로 유저정보찾기")
    public Object findUserByEmail(@PathVariable String email)throws Exception{
        User user = service.findUserByEmail(email);
    	final BasicResponse result = new BasicResponse();
    	JSONObject dummyUser = new JSONObject();
    	dummyUser.put("uid", user.getUid());
        dummyUser.put("email", user.getEmail());
        dummyUser.put("intro", user.getIntro());
        dummyUser.put("nickname", user.getNickname());
        dummyUser.put("profileimg", user.getProfileimg());
        dummyUser.put("usertitle", user.getUsertitle());
        dummyUser.put("createdate", user.getcreatedate());

    	result.status = true;
    	result.data = "succ";
    	result.object = dummyUser.toMap();
    	return new ResponseEntity<>(result,HttpStatus.OK);
    }
    
    @PostMapping("/account/findUserByNickname/{nickname}")
    @ApiOperation(value = "nickname로 유저정보찾기")
    public Object findUserByNickname(@PathVariable String nickname)throws Exception{
        System.out.println(nickname);
        User user = service.findUserByNick(nickname);
    	final BasicResponse result = new BasicResponse();
    	JSONObject dummyUser = new JSONObject();
    	dummyUser.put("uid", user.getUid());
        dummyUser.put("email", user.getEmail());
        dummyUser.put("intro", user.getIntro());
        dummyUser.put("nickname", user.getNickname());
        dummyUser.put("profileimg", user.getProfileimg());
        dummyUser.put("usertitle", user.getUsertitle());
        dummyUser.put("createdate", user.getcreatedate());

    	result.status = true;
    	result.data = "succ";
    	result.object = dummyUser.toMap();
    	return new ResponseEntity<>(result,HttpStatus.OK);
    }
    
    @PostMapping("/account/findUserByUid/{uid}")
    @ApiOperation(value = "uid 유저정보찾기")
    public Object findUserByUid(@PathVariable int uid)throws Exception{
        System.out.println(uid);
        User user = service.findUserByUid(uid);
    	final BasicResponse result = new BasicResponse();
    	
    	JSONObject dummyUser = new JSONObject();
    	
    	dummyUser.put("uid", user.getUid());
        dummyUser.put("email", user.getEmail());
        dummyUser.put("intro", user.getIntro());
        dummyUser.put("nickname", user.getNickname());
        dummyUser.put("profileimg", user.getProfileimg());
        dummyUser.put("usertitle", user.getUsertitle());
        dummyUser.put("createdate", user.getcreatedate());

    	result.status = true;
    	result.data = "succ";
    	result.object = dummyUser.toMap();
    	return new ResponseEntity<>(result,HttpStatus.OK);
    }
    
}	