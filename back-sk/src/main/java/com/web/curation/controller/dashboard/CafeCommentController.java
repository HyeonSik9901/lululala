package com.web.curation.controller.dashboard;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.web.curation.model.BasicResponse;
import com.web.curation.model.dashboard.CafeComment;
import com.web.curation.model.dashboard.CafeRecomment;
import com.web.curation.model.dashboard.Comment;
import com.web.curation.model.dashboard.Recomment;
import com.web.curation.model.user.User;
import com.web.curation.service.dashboard.CafeCommentService;
import com.web.curation.service.dashboard.CafeRecommentService;
import com.web.curation.service.dashboard.CommentService;
import com.web.curation.service.dashboard.RecommentService;
import com.web.curation.service.user.UserDaoService;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/cafecomment")
public class CafeCommentController {

	@Autowired
	private CafeCommentService cafecommentService;
	@Autowired
	private CafeRecommentService caferecommentService;
	@Autowired
	private UserDaoService userdaoservice;
	
	
	
	@PostMapping("/insertCafeComment")
	@ApiOperation(value = "카페댓글삽입")
	public ResponseEntity<BasicResponse> insertCafeComment(@RequestBody CafeComment cafecomment)
			throws Exception {

		System.out.println("1코멘트 삽입확인");
		boolean flag = cafecommentService.insertCafeComment(cafecomment);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("삽입오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@PostMapping("/insertCafeRecomment")
	@ApiOperation(value = "카페댓글의댓글")
	public ResponseEntity<BasicResponse> insertCafeRecomment(@RequestBody CafeRecomment caferecomment) throws Exception {

		System.out.println("코멘트의 코멘트 삽입확인");
		boolean flag = caferecommentService.insertCafeRecomment(caferecomment);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("코멘트오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@PutMapping("/updateCafeComment")
	@ApiOperation(value = "카페댓글수정")
	public ResponseEntity<BasicResponse> updateCafeComment(@RequestBody CafeComment cafecomment) throws Exception {
		boolean flag = cafecommentService.updateCafeComment(cafecomment);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("업데이트오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@PutMapping("/updateCafeRecomment")
	@ApiOperation(value = "카페댓글의 댓글 수정")
	public ResponseEntity<BasicResponse> updateCafeRecomment(@RequestBody CafeRecomment caferecomment) throws Exception {
		boolean flag = caferecommentService.updateCafeRecomment(caferecomment);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("댓글의 댓글 업데이트오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@DeleteMapping("/deleteCafeComment/{cid}")
	@ApiOperation(value = "카페댓글삭제")
	public ResponseEntity<BasicResponse> deleteCafeComment(@PathVariable int cid) throws Exception {
		boolean flag = cafecommentService.deleteCafeComment(cid);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("삭제오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}

		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@DeleteMapping("/deleteCafeRecomment/{recid}")
	@ApiOperation(value = "카페댓글의댓글삭제")
	public ResponseEntity<BasicResponse> deleteCafeRecomment(@PathVariable int recid) throws Exception {
		boolean flag = caferecommentService.deleteCafeRecomment(recid);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("댓글의 댓글삭제오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}

		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@PostMapping("/searchCafeComment/{cfid}")
	@ApiOperation(value = "카페댓글보기")
	public ResponseEntity<BasicResponse> searchCafeComment(@PathVariable int cfid) throws Exception {
		List<CafeComment> cafecomment = cafecommentService.searchCafeComment(cfid);
		BasicResponse result = new BasicResponse();
		System.out.println(cafecomment.toString());
		JSONObject dummy = new JSONObject();
		List<CafeComment> temp = new ArrayList<CafeComment>();
		
		for (int i = 0; i < cafecomment.size(); i++) {
			int count = caferecommentService.countCafeRecomment(cafecomment.get(i).getCid());
			User tempuser = userdaoservice.findUserByUid(cafecomment.get(i).getUid());
			CafeComment tempCafeComment = new CafeComment(cafecomment.get(i).getCid(), cafecomment.get(i).getUid(), 
					cafecomment.get(i).getFid(), cafecomment.get(i).getContents(), cafecomment.get(i).getCreateDate(), count, tempuser.getProfileimg(),tempuser.getNickname());
			temp.add(tempCafeComment);
		}
		dummy.put("cafecommentlist", temp);
		if (cafecomment.isEmpty()) {
			result.status = false;
			result.data = "fail";
			return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
		} else {
			System.out.println("코멘트있음");
			result.status = true;
			result.data = "succ";
		}
		result.object = dummy.toMap();

		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);

	}

	@PostMapping("/searchCafeRecomment/{cid}")
	@ApiOperation(value = "카페댓글의댓글보기")
	public ResponseEntity<BasicResponse> searchCafeRecomment(@PathVariable int cid) throws Exception {
		List<CafeRecomment> caferecomment = caferecommentService.searchCafeRecomment(cid);
		BasicResponse result = new BasicResponse();
		JSONObject dummy = new JSONObject();
		
		List<CafeRecomment> temp = new ArrayList<CafeRecomment>();
		
		for (int i = 0; i < caferecomment.size(); i++) {
			User tempuser = userdaoservice.findUserByUid(caferecomment.get(i).getUid());
			
			CafeRecomment tempCafeComment = new CafeRecomment(caferecomment.get(i).getRecid(), caferecomment.get(i).getCid(), 
					caferecomment.get(i).getUid(), caferecomment.get(i).getContents(), caferecomment.get(i).getCreateDate(), tempuser.getProfileimg(),tempuser.getNickname());
			
			temp.add(tempCafeComment);
		}
		
		dummy.put("caferecommentlist", temp);
		
		if (caferecomment.isEmpty()) {
			System.out.println("재댓글 없음");
			result.status = false;
			result.data = "fail";
			return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);

		} else {
			System.out.println("댓글의 댓글있음");
			result.status = true;
			result.data = "succ";
		}
		
		result.object = dummy.toMap();
		System.out.println(caferecomment);
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

}