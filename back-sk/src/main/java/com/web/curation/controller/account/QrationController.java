package com.web.curation.controller.account;

import java.util.List;
import java.util.StringTokenizer;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.web.curation.model.BasicResponse;
import com.web.curation.model.user.Quration;
import com.web.curation.service.dashboard.HashtagService;
import com.web.curation.service.dashboard.KeywordService;
import com.web.curation.service.user.QurationDaoService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ApiResponses(value = { @ApiResponse(code = 401, message = "Unauthorized", response = BasicResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = BasicResponse.class),
		@ApiResponse(code = 404, message = "Not Found", response = BasicResponse.class),
		@ApiResponse(code = 500, message = "Failure", response = BasicResponse.class) })

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/quration")
public class QrationController {

	@Autowired
	QurationDaoService qurationDaoService;

	@Autowired
	KeywordService keywordService;

	@Autowired
	HashtagService hashtagService;
	
	
	@GetMapping("/searchAllQuration/{uid}")
	@ApiOperation(value = "큐레이션가져오기")
	public Object searchAllQuration(@PathVariable int uid)throws Exception{
		BasicResponse result = new BasicResponse();
		List<Quration> qurationlist = qurationDaoService.searchAllQuration(uid);
		System.out.println("quration List :: " + qurationlist);
		JSONObject dummy = new JSONObject();
		dummy.put("qurationlist", qurationlist);
		if(qurationlist.isEmpty()) {
			System.out.println("큐레이션 아에 없음");
			result.status = false;
			result.data = "fail";
			return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
		}else {
			System.out.println("큐레이션 있음");
			result.status = true;
			result.data = "succ";
		}
		result.object = dummy.toMap();
		return new ResponseEntity<BasicResponse>(result,HttpStatus.OK);
	}
	//searchQuration
	
	@PostMapping("/searchQuration")
	@ApiOperation(value = "큐레이션미리출력하기")
	public Object searchQuration(@RequestBody Quration quration)throws Exception{
		BasicResponse result = new BasicResponse();
		
		List<String> tagList = qurationDaoService.searchQuration(quration);
		
		JSONObject dummy = new JSONObject();
		dummy.put("qurationlist", tagList);
		if(tagList.isEmpty()) {
			System.out.println("키워드 아에 없음");
			result.status = false;
			result.data = "fail";
			return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
		}else {
			System.out.println("키워드 있음");
			result.status = true;
			result.data = "succ";
		}
		result.object = dummy.toMap();
		return new ResponseEntity<BasicResponse>(result,HttpStatus.OK);
	}
	
	
	@PostMapping("/insertQuration")
	@ApiOperation(value = "큐레이션등록")
	public Object insertQuration(@RequestBody Quration quration)
			throws Exception {
		// 키워드 ,로 구분하고 키워드 여러개 들어오면 >>> 해쉬태그에 있는키워드있는지 없는 키워드 인지검사하고
		// 해쉬태그에 있으면 그거 쓰고 없으면 넣어주고 키워드를
		// 키워드 DB : 큐레이션id, 해쉬태그id
		// 큐레이션DB : 유저id , 큐레이션이름name
		
		
		String keyword = quration.getTag();
		BasicResponse result = new BasicResponse();
		System.out.println("여기오나 지금 실험중 2월17일");
		Quration TempQuration = new Quration(quration.getUid(),quration.getName());
		//큐레이션 체크 메소드 만들어서>> 만약에 이미 있으면 지우고 만들고 없는거라면 그냥 바로 만들기
		boolean deleteCheck = qurationDaoService.checkQuration(TempQuration);
		if(deleteCheck) {
			boolean deleteQuration = qurationDaoService.deleteQuration(TempQuration);
			if(deleteQuration) {
				System.out.println("삭제성공");
			}else {
				System.out.println("삭제실패");
			}
		}
		//트루가 아니면 삭제안해주고 바로 큐레이션 인서트 들어감
		
		boolean flag = qurationDaoService.insertQuration(TempQuration);
		System.out.println("keyword" + " " + keyword);
		System.out.println("quration" + " " + TempQuration);
		if (flag) {// 들어갔으면 키워드 넣는 작업 시작.

			if (keyword != null) {
				System.out.println("와라여기");
				StringTokenizer st = new StringTokenizer(keyword, ",");
				while (st.hasMoreTokens()) {
					String word = st.nextToken();
					boolean hashtagCheck = hashtagService.searchHashtagByCheck(word);
					System.out.println("와라여기2");
					Quration keywordTo = new Quration(quration.getUid(), quration.getName(), word);
					if (hashtagCheck) {// 여기 들어오면 디비에 처음 들어오는 키워드라서 등록해준다.
						boolean insertHashtag = hashtagService.insertHashtag(word);
						if (!insertHashtag) {
							System.out.println("Hashtag추가 실패!");
						}
						boolean insertFlag = keywordService.insertKeyword(keywordTo);
						if (insertFlag) {
							System.out.println("키워드 추가 성공");
						} else {
							System.out.println("키워드 추가 실패");
						}
					} else {// 여기 들어오면 디비에 이미 있는 키워드라서 등록할 필요없이 넣어준다.
						boolean insertFlag = keywordService.insertKeyword(keywordTo);
						if (insertFlag) {
							System.out.println("키워드 추가 성공2");
						} else {
							System.out.println("키워드 추가 실패2");
						}
					}
				}
			}
			result.status = true;
			result.data = "succ";
		} else {
			System.out.println("qration삽입실패");
			result.status = false;
			result.data = "fail";
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	//메소드 추가 부분
	@PostMapping("/updateQuration")
	@ApiOperation(value = "큐레이션수정")
	public Object updateQuration(@RequestBody Quration quration, @RequestParam(required = false) String keyword)
			throws Exception {
		
		
		BasicResponse result = new BasicResponse();
		boolean qurationflag = qurationDaoService.updateQuration(quration);//이름을 바꾸는 작업만 하는 큐레이션 
		if(!qurationflag) {
			System.out.println("키워드만 바꾸는 작업함");
		}else {
			System.out.println("키워드와 큐레이션 이름도 바꾸는 작업");
		}
		boolean keywordDelete = keywordService.deleteKeyword(quration.getQid());
		if(!keywordDelete) {
			//여기서부터 삭제 됬으면 true 안됬으면 false 
			//키워드가 삭제가 안됬음
			//전부 삭제하고 다시 삽입하는 과정임
			System.out.println("키워드가 삭제가 안됬습니다.");
			//원래 키워드가 없었는데 수정할때 추가했을수도 있다.
		}
			if(keyword != null) {
				StringTokenizer st = new StringTokenizer(keyword,",");
				while(st.hasMoreTokens()) {
					String word = st.nextToken();
					boolean hashtagCheck = hashtagService.searchHashtagByCheck(word);
					//새로 입력한 키워드가 해쉬태그에 없는 키워드다 -> 키워드 추가하는 작업 다시 시작
					Quration keywordTo = new Quration(quration.getUid(), quration.getName(), word);
					if(hashtagCheck) {//해쉬태그에 없는 키워드다 그래서 추가하고 내 큐레이션 수정한다.
						boolean insertHashtag = hashtagService.insertHashtag(word);
						if(!insertHashtag) {
							System.out.println("해쉬태그 입력에 실패했습니다.");
						}
						boolean insertFlag = keywordService.insertKeyword(keywordTo);
						//새로운 키워드 큐레이션에 넣는다.
						if(insertFlag) {
							System.out.println("큐레이션 키워드 수정 성공");
						}else {
							System.out.println("큐레이션 키워드 수정 실패");
						}
					}else {
						boolean insertFlag = keywordService.insertKeyword(keywordTo);
						if (insertFlag) {
							System.out.println("키워드 추가 성공2");
						} else {
							System.out.println("키워드 추가 실패2");
						}
					}
				}
			
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	//큐레이션 삭제시 키워드까지 전부 지운다.
	@DeleteMapping("/deleteQuration")
	@ApiOperation(value = "큐레이션삭제")
	public Object deleteQuration(@RequestBody Quration quration)
			throws Exception {	
		BasicResponse result = new BasicResponse();
		
		boolean qurationflag = qurationDaoService.deleteQuration(quration);
		if(qurationflag) {	//삭제가 완료되었음.
			result.status = true;
			result.data = "succ";
		}else {//삭제 실패
			result.status = false;
			result.data = "fail";
		}
		
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	
	
}
