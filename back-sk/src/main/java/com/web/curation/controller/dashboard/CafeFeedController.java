package com.web.curation.controller.dashboard;

import java.io.File;
import java.util.UUID;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.web.curation.dao.dashboard.CafeDao;
import com.web.curation.model.BasicResponse;
import com.web.curation.model.dashboard.CafeFeed;
import com.web.curation.model.dashboard.Comment;
import com.web.curation.service.dashboard.CafeFeedService;
import com.web.curation.service.dashboard.CafeService;
import com.web.curation.service.user.CafeUserDaoService;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/cafefeed")
public class CafeFeedController {
	
	@Autowired
	private CafeUserDaoService cafeuserdaoservice;
	
	@Autowired
	private CafeService cafeservice;
	
	@Autowired
	private CafeFeedService cafefeedservice;
	
	@PostMapping("/uploadfilecafefeed")
	@ApiOperation(value = "카페사진업로드")
	public Object uploadfilecafefeed(@RequestPart MultipartFile postImage) throws Exception {
		MultipartFile file = postImage;
		String filename = file.getOriginalFilename();
		String sub=filename;
		char[] arr = filename.toCharArray();
		for(int i=0;i<filename.length();i++){
			if(arr[i] == '.'){
				i++;
				sub = sub.substring(i);
				break;
			}
		}
		

		
		BasicResponse result = new BasicResponse();
		
		UUID uuid = UUID.randomUUID();
		String path = "C:\\Users\\multicampus\\Desktop\\git\\sns-sprint3\\back-sk\\src\\main\\resources\\static\\img\\"+uuid+"."+sub;
		File copyFile = new File(path);
		
		System.out.println(copyFile.getPath());

		file.transferTo(copyFile);
		path = "http://192.168.100.72:8080/lululala/img/"+uuid+"."+sub;
		System.out.println(path);
		result.status = true;
		result.data = path;
		
		return new ResponseEntity<>(result, HttpStatus.OK);

	}
	
	@PostMapping("/insertCafeFeed")
	@ApiOperation(value = "카페피드삽입")
	public ResponseEntity<BasicResponse> insertCafeFeed(@RequestBody CafeFeed cafefeed)
			throws Exception {

		System.out.println("카페피드 삽입확인");
		boolean flag = cafefeedservice.insertCafeFeed(cafefeed);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("삽입오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@PutMapping("/updateCafeFeed")
	@ApiOperation(value = "카페피드수정")
	public ResponseEntity<BasicResponse> updateCafeFeed(@RequestBody CafeFeed cafefeed) throws Exception {
		boolean flag = cafefeedservice.updateCafeFeed(cafefeed);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("업데이트오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	@DeleteMapping("/deleteCafeFeed/{cfid}")
	@ApiOperation(value = "카페피드삭제")
	public ResponseEntity<BasicResponse> deleteCafeFeed(@PathVariable int cfid) throws Exception {
		boolean flag = cafefeedservice.deleteCafeFeed(cfid);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("삭제오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}

		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	@PostMapping("/searchAllCafeFeed/{cafeid}") //카페글 보기
	@ApiOperation(value = "카페글보기")
	public Object searchAllCafeFeed(@PathVariable int cafeid) throws Exception {
		System.out.println(cafeid+"번 카페피드 출력");
		List<CafeFeed> list = cafefeedservice.searchAllCafeFeed(cafeid);
		BasicResponse result = new BasicResponse();
		
		JSONObject dummy = new JSONObject();
		dummy.put("cafefeedlist", list);
		if (list.isEmpty()) {
			System.out.println("글 아에 없음");
			result.status = false;
			result.data = "fail";
			return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
		} else {
			System.out.println("글있음");
			result.status = true;
			result.data = "succ";
		}
		result.object = dummy.toMap();
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
}
