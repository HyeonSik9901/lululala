package com.web.curation.controller.dashboard;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.web.curation.model.BasicResponse;
import com.web.curation.model.user.Likes;
import com.web.curation.service.dashboard.LikesService;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/like")
public class LikesController {

    @Autowired
    LikesService service;

	@PostMapping("/insertlikes") //좋아요 추가
	@ApiOperation(value = "좋아요 추가")
	public Object insertlikes(@RequestBody Likes likes) throws Exception {
        BasicResponse result = new BasicResponse();
        JSONObject dummy = new JSONObject();

        Likes like = service.checklikefid(likes);
		
        Boolean flag = true;
        if(like == null){
			flag = false;
			service.insertLikes(likes);
			result.status = true;
			result.data = "succ";		
			result.object = dummy.toMap();		
		}else{
			//System.out.println(flag);
			result.status = true;
			result.data = "fail";
			result.object = dummy.toMap();
		}
        
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
    @PostMapping("/delectlikes") //좋아요 추가
	@ApiOperation(value = "좋아요 추가")
	public Object delectlikes(@RequestBody Likes likes) throws Exception {
        BasicResponse result = new BasicResponse();
        JSONObject dummy = new JSONObject();

        Likes like = service.checklikefid(likes);
		
        Boolean flag = true;
        if(like != null){
			flag = false;
			service.deleteLikes(likes);
			result.status = true;
			result.data = "succ";		
			result.object = dummy.toMap();		
		}else{
			System.out.println(flag);
			result.status = true;
			result.data = "fail";
			result.object = dummy.toMap();
		}
        
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
    @PostMapping("/checklikefid") //좋아요 됬는지 확인
	@ApiOperation(value = "fid 와 uid 로 좋아오 여부 확인")
	public Object checklikefid(@RequestBody Likes likes) throws Exception {
        
        
        BasicResponse result = new BasicResponse();
        JSONObject dummy = new JSONObject();

        Likes like = service.checklikefid(likes);
		
        Boolean flag = true;
        if(like == null){
			flag = false;
			System.out.println(flag);
			result.status = true;
			result.data = "fail";
			result.object = dummy.toMap();
			
		}else{
			System.out.println(flag);
			result.status = true;
			result.data = "succ";
			
			result.object = dummy.toMap();
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	@PostMapping("/countlikes") //좋아요 갯수 확인
	@ApiOperation(value = "fid 로 좋아요 숫자 확인")
	public Object countlikes(@RequestBody Likes likes) throws Exception {
        
        
        BasicResponse result = new BasicResponse();
        JSONObject dummy = new JSONObject();

        int cnt = service.searchLikes(likes.getFid());
		System.out.println(likes.getFid()+"번 피드의 좋아요 숫자는 :"+cnt);
        dummy.put("count",cnt);
		System.out.println(cnt);
		result.status = true;
		result.data = "succ";
		
		result.object = dummy.toMap();
			
		
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	 	
	 	
	 	
	 	
}
