package com.web.curation.controller.dashboard;

import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.web.curation.model.BasicResponse;
import com.web.curation.model.lala.Cafe;
import com.web.curation.service.dashboard.CafeService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@ApiResponses(value = { @ApiResponse(code = 401, message = "Unauthorized", response = BasicResponse.class),
		@ApiResponse(code = 403, message = "Forbidden", response = BasicResponse.class),
		@ApiResponse(code = 404, message = "Not Found", response = BasicResponse.class),
		@ApiResponse(code = 500, message = "Failure", response = BasicResponse.class) })

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/cafe")
public class CafeController {

	@Autowired
	private CafeService cafeService;
	
	@PostMapping("/insertCafe")
	@ApiOperation(value = "카페추가")
	public ResponseEntity<BasicResponse> insertCafe(@RequestBody Cafe cafe)
			throws Exception {

		System.out.println("카페추가");
		boolean flag = cafeService.insertCafe(cafe);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("삽입오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	
	@PutMapping("/updateCafe")
	@ApiOperation(value = "카페수정")
	public ResponseEntity<BasicResponse> updateCafe(@RequestBody Cafe cafe) throws Exception {
		boolean flag = cafeService.updateCafe(cafe);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("카페수정");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	@DeleteMapping("/deleteCafe/{cafeid}")
	@ApiOperation(value = "카페삭제")
	public ResponseEntity<BasicResponse> deleteCafe(@PathVariable int cafeid) throws Exception {
		boolean flag = cafeService.deleteCafe(cafeid);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("삭제오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}

		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	@PostMapping("/searchOneCafeById/{cafeid}")
	@ApiOperation(value = "카페상세정보보기")
	public ResponseEntity<BasicResponse> searchOneCafeById(@PathVariable int cafeid) throws Exception {
		
		Cafe cafe = cafeService.searchOneCafeById(cafeid);
		//먼저 카페id로 카페 상세정보를 가져온다음
		
		//상세정보를 봤기때문에 조회수+1 해주는 작업도 한다.
		boolean flag = cafeService.countPlusCafe(cafe);
		
		JSONObject dummy = new JSONObject();
		
		dummy.put("cafe", cafe);
		BasicResponse result = new BasicResponse();
		result.object = dummy.toMap();		
		
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	@PostMapping("/searchAllCafe") // 전체 카페목록띄우기
	@ApiOperation(value = "전체카페목록")
	public Object searchAllCafe() throws Exception {
		List<Cafe> list = cafeService.searchAllCafe();
		BasicResponse result = new BasicResponse();
		
		JSONObject dummy = new JSONObject();
		dummy.put("cafelist", list);
		if (list.isEmpty()) {
			System.out.println("카페 목록이 없음");
			result.status = false;
			result.data = "fail";
			return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
		} else {
			System.out.println("카페목록들어옴");
			result.status = true;
			result.data = "succ";
		}
		result.object = dummy.toMap();
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	@PostMapping("/searchMyCafe/{uid}") //내가 가입된 카페목록띄우기
	@ApiOperation(value = "가입된카페")
	public Object searchMyCafe(@PathVariable int uid) throws Exception {
		System.out.println(uid+"번 내 피드보기");
		List<Cafe> list = cafeService.searchMyCafe(uid);
		BasicResponse result = new BasicResponse();
		
		JSONObject dummy = new JSONObject();
		dummy.put("cafelist", list);
		if (list.isEmpty()) {
			System.out.println("가입된 카페 아에 없음");
			result.status = false;
			result.data = "fail";
			
			return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);
		} else {
			System.out.println("가입된 카페있음");
			result.status = true;
			result.data = "succ";
		}
		result.object = dummy.toMap();
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}
	
	
	
}
