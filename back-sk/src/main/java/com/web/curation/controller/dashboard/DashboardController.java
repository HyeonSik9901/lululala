package com.web.curation.controller.dashboard;

//import javax.validation.Valid;

//import com.web.curation.dao.user.UserDao;
import com.web.curation.model.BasicResponse;
import com.web.curation.model.user.SignupRequest;
//import com.web.curation.model.user.User;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiOperation;


@ApiResponses(value = {
    @ApiResponse(code = 401, message = "Unauthorized", response = BasicResponse.class),
    @ApiResponse(code = 403, message = "Forbidden", response = BasicResponse.class),
    @ApiResponse(code = 404, message = "Not Found", response = BasicResponse.class),
    @ApiResponse(code = 500, message = "Failure", response = BasicResponse.class)})
    
@RestController 
@CrossOrigin(origins="*")
public class DashboardController{
    // 디비 셋팅 후 주석을 푸세요.
    // @Autowired
    // UserDao userDao;

    @PostMapping("/dashboard/createboard")
    @ApiOperation(value = "게시판생성")
    public Object createboard(@RequestParam(required = true) final String email,
                        @RequestParam(required = true) final String password){

        
        JSONObject dummyUser = new JSONObject();
        
        System.out.println(email);
        System.out.println(password);
        
        
        
        dummyUser.put("uid","test_uid");
        dummyUser.put("email","test@test.com");
        dummyUser.put("nickname","test_nickname");
        
        
        final BasicResponse result = new BasicResponse();
        result.status = true;
        result.data = "success";
        result.object = dummyUser.toMap();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/dashboard/insertboard")
    @ApiOperation(value = "게시물생성")
    public Object insertboard(@RequestBody SignupRequest request) {
        //이메일, 닉네임 중복처리 필수
        //회원가입단을 생성해 보세요.
    	
    	System.out.println(request.getEmail());
    	System.out.println(request.getNickname());
    	System.out.println(request.getPassword());
    	final BasicResponse result = new BasicResponse();
        result.status = true;
        result.data = "succ";
        System.out.println(result.data);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    
    

    
}