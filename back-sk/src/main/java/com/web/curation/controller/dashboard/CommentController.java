package com.web.curation.controller.dashboard;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.web.curation.model.BasicResponse;
import com.web.curation.model.dashboard.Comment;
import com.web.curation.model.dashboard.Recomment;
import com.web.curation.model.user.User;
import com.web.curation.service.dashboard.CommentService;
import com.web.curation.service.dashboard.RecommentService;
import com.web.curation.service.user.UserDaoService;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/comment")
public class CommentController {

	@Autowired
	private CommentService commentService;
	@Autowired
	private RecommentService recommentService;
	@Autowired
	private UserDaoService userdaoservice;
	
	
	
	@PostMapping("/insertComment")
	@ApiOperation(value = "댓글삽입")
	public ResponseEntity<BasicResponse> insertComment(@RequestBody com.web.curation.model.dashboard.Comment comment)
			throws Exception {

		System.out.println("1코멘트 삽입확인");
		boolean flag = commentService.insertComment(comment);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("삽입오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@PostMapping("/insertRecomment")
	@ApiOperation(value = "댓글의댓글")
	public ResponseEntity<BasicResponse> insertRecomment(@RequestBody Recomment recomment) throws Exception {

		System.out.println("코멘트의 코멘트 삽입확인");
		boolean flag = recommentService.insertRecomment(recomment);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("코멘트오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@PutMapping("/updateComment")
	@ApiOperation(value = "댓글수정")
//	@RequestMapping(value = "/updateComment", method = RequestMethod.PUT)
	public ResponseEntity<BasicResponse> updateComment(@RequestBody Comment comment) throws Exception {
		boolean flag = commentService.updateComment(comment);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("업데이트오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@PutMapping("/updateRecomment")
	@ApiOperation(value = "댓글의 댓글 수정")
//	@RequestMapping(value = "/updateComment", method = RequestMethod.PUT)
	public ResponseEntity<BasicResponse> updateRecomment(@RequestBody Recomment recomment) throws Exception {
		boolean flag = recommentService.updateRecomment(recomment);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("댓글의 댓글 업데이트오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@DeleteMapping("/deleteComment/{cid}")
	@ApiOperation(value = "댓글삭제")
//	@RequestMapping(value = "/deleteComment/{cid}", method = RequestMethod.DELETE)
	public ResponseEntity<BasicResponse> deleteComment(@PathVariable int cid) throws Exception {
		boolean flag = commentService.deleteComment(cid);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("삭제오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}

		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@DeleteMapping("/deleteRecomment/{recid}")
	@ApiOperation(value = "댓글의댓글삭제")
//	@RequestMapping(value = "/deleteComment/{cid}", method = RequestMethod.DELETE)
	public ResponseEntity<BasicResponse> deleteRecomment(@PathVariable int recid) throws Exception {
		boolean flag = recommentService.deleteRecomment(recid);
		BasicResponse result = new BasicResponse();
		if (!flag) {
			System.out.println("댓글의 댓글삭제오류");
			result.status = false;
			result.data = "fail";
		} else {
			result.status = true;
			result.data = "succ";
		}

		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

	@PostMapping("/searchComment/{fid}")
	@ApiOperation(value = "댓글보기")
	public ResponseEntity<BasicResponse> searchComment(@PathVariable int fid) throws Exception {
		List<Comment> comment = commentService.searchComment(fid);
		BasicResponse result = new BasicResponse();
		
		System.out.println(comment.toString());
		
		JSONObject dummy = new JSONObject();
		
		//dummy.put("commentlist", comment);

		List<Comment> temp = new ArrayList<Comment>();
		
		for (int i = 0; i < comment.size(); i++) {
			int count = recommentService.countRecomment(comment.get(i).getCid());
			User tempuser = userdaoservice.findUserByUid(comment.get(i).getUid());
			//System.out.println(tempuser.getProfileimg());
			Comment tempComment = new Comment(comment.get(i).getCid(), comment.get(i).getUid(), 
					comment.get(i).getFid(), comment.get(i).getContents(), comment.get(i).getCreateDate(), count, tempuser.getProfileimg(),tempuser.getNickname());
			//System.out.println(i + " :: " + tempComment);
			temp.add(tempComment);
			
		//	dummy.put("commentNum", comment.get(i).getCid());
		//	dummy.put("recommentCount", count);
			//Comment tempComment = comment.get(i);
		//	dummy.put("comment", tempComment);
		}
		dummy.put("commentlist", temp);
		
		if (comment.isEmpty()) {
			System.out.println("코멘트 없음");
			result.status = false;
			result.data = "fail";
			return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);

		} else {
			System.out.println("코멘트있음");
			result.status = true;
			result.data = "succ";
		}
		result.object = dummy.toMap();

		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);

	}

	@PostMapping("/searchRecomment/{cid}")
	@ApiOperation(value = "댓글의댓글보기")
	public ResponseEntity<BasicResponse> searchRecomment(@PathVariable int cid) throws Exception {
		List<Recomment> recomment = recommentService.searchRecomment(cid);
		BasicResponse result = new BasicResponse();
		JSONObject dummy = new JSONObject();
		
		List<Recomment> temp = new ArrayList<Recomment>();
		
		for (int i = 0; i < recomment.size(); i++) {
			User tempuser = userdaoservice.findUserByUid(recomment.get(i).getUid());
			
			Recomment tempComment = new Recomment(recomment.get(i).getRecid(), recomment.get(i).getCid(), 
					recomment.get(i).getUid(), recomment.get(i).getContents(), recomment.get(i).getCreateDate(), tempuser.getProfileimg(),tempuser.getNickname());
			
			temp.add(tempComment);
		}
		
		dummy.put("recommentlist", temp);
		
		if (recomment.isEmpty()) {
			System.out.println("재댓글 없음");
			result.status = false;
			result.data = "fail";
			return new ResponseEntity<BasicResponse>(HttpStatus.NO_CONTENT);

		} else {
			System.out.println("댓글의 댓글있음");
			result.status = true;
			result.data = "succ";
		}
		
		result.object = dummy.toMap();
		System.out.println(recomment);
		return new ResponseEntity<BasicResponse>(result, HttpStatus.OK);
	}

}