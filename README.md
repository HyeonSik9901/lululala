###  [공통-Sub PJT Ⅲ]


| **학습 형태**  | []개별 [ ] 5인 1조 [■ ] 팀별                 |
| :------------: | -------------------------------------------- |
|   **조원명**   | 문현식(조장), 이희선, 박재동, 박찬환, 김대훈 |
|    **기간**    | 200203 ~ 200303                              |
| **프로젝트명** | 룰루랄라🤸                                    |
|    **주제**    | [웹/모바일 프로젝트] 웹 큐레이션 SNS         |
|    **내용**    | 카페기능의 SNS 페이지 구성                   |


## 🎼 frontend

1. Yarn 설치
   https://yarnpkg.com/en/docs/install#windows-stable 윈도우 버전 다운로드

2. Vue-cli 설치

  ```yarn global add @vue/cli```

  ```vue -version```

3. 프로젝트설치
   ```vue create {project name}```

4. Sass 설정
   ```yarn add node-sass sass-loader```

5. Vue-router, Vuex 설치
   ```yarn add vue-router vuex```

6. 기타 기능추가를 위한 설치

   ```
   yarn --serve
   ```

   위의 명령은 아래의 내용을 포함합니다

   ```bash
   yarn add vue-material
   yarn add vuex
   yarn add vue-awesome-swiper
   yarn add vue-nav-tabs
   yarn add babel-helper-vue-jsx-merge-props
   yarn add vue-material
   yarn add @johmun/vue-tags-input
   yarn add bootstrap-vue
   yarn add vue-infinite-loading -S
   yarn add vue-social-sharing
   ```

7. Node.js기반의 로컬 웹 서버 실행
   ```yarn serve --port 3000```또는``yarn serve --port 3000 --fix``


## 🎧 backend

1.Maven 다운 받고 환경변수 설정
http://maven.apache.org/download.cgi 에서 zip파일 다운

2.설치확인

cmd창에서 mvn -v 실행하여 설치한 버전이 정상적으로 뜨는 지 확인

3.VScode설치
https://code.visualstudio.com/docs/java/java-spring-boot

4.VScode 내부에서 Spring Boot Extansion Pack 설치

5.back-sk/pom.xml에서 f5를 누른뒤 java를 선택하여 실행

## 🎹 UI

1.로그인 작은화면
<br>
![login_sm](/uploads/2f3680508bea331717b19d4979346ad1/login_sm.png)

2.로그인 큰화면
<br>
![로그인큰화면](/uploads/1a217ac7cadd87fc08adc884af668326/로그인큰화면.png)

로그인 화면에서 로그인을 하거나 회원가입을 하실 수 있습니다.



3.피드
<br>
![피드작은화면](/uploads/b7a088044ae7d0a3547b6cf7bc073e7e/피드작은화면.png)

4.피드 큰화면
<br>
![피드](/uploads/1c43dd9565572cfc1dfdfe4af0da2d45/피드.png)

피드 화면에서는 로그인한 사용자의 정보과 사용자가 태그된 글들을 모아보이과 자세히보기로 볼 수 있으며, 모아보기에서는 각각의 사진을 클릭하여 상세화면을 열어 정보를 볼 수 있습니다.



5.피드 상세화면
<br>
![피드상세화면](/uploads/59bd1d9ab6dd0254f3887946580525c0/피드상세화면.png)

상세화면에서는 피드내용과 댓글을 볼 수 있으며, 새로운 댓글을 달거나 댓글을 단 유저의 정보를 볼 수 있습니다.



6.프로필 팔로우 팔로워 화면
<br>
![내프로필](/uploads/b5a45784a82fc8fc563cc873e964da4c/내프로필.png)

프로필 화면에서는 자신의 프로필을 볼 수 있으며, 작성한 피드와 내가 태그 된 피드를 보거나 새로운 피드를 작성할 수 있습니다.



7.사이드바
<br>
![사이드바](/uploads/02319cff82b0c95bccd6d4c5c9c750da/사이드바.png)

좌측상단 버튼을 눌러 사이드바를 호출할 수 있으며, 가입된 랄라(카페)와 큐레이션을 수정할 수 있습니다.



8.회원가입
<br>
![회원가입화면](/uploads/7cb842ad700ed87addbe7deb9a46c7f2/회원가입화면.png)

간단한 정보를 입력하여 회원가입을 하실 수 있으며, 메일인증을 필수로 해야 합니다.



9.회원정보 수정
<br>
![회원정보수정화면](/uploads/7115993e1323369c5acd41d830f5faa2/회원정보수정화면.png)

이메일을 제외한 나머지정보를 수정할 수 있습니다.



10.카페 화면
<br>
![카페화면](/uploads/87dff240b86715fa5f25eb5710ae877a/카페화면.png)

카페에서 글을 쓰거나 작성된 글에 들어가서 내용을 보고 댓글을 달 수 있습니다.



## 🎤 프로젝트 소개

- 인터넷 URL 기반의 공유 SNS 입니다.
- SNS 기능으로 공개 설정에따라 원하는 사용자에게만 제공합니다.
- 취미가 같은 사람들끼리 소통하고 정보를 공유하며 예전 감성을 자극하는 SNS입니다.
- 복잡하지않고 간단하며 사람들끼리 취미를 소통하는 국내 최초 감성형 SNS 룰루랄라 입니다.
- 태그로 사람이나 모든 해쉬태그를 검색하고 공유 할 수 있는 SNS입니다.

## 😆 룰루랄라의 강점

- 내가 등록해놓은 관심사들을 통해서 서로 정보를 공유할 수 있습니다.
- 기존 SNS와 다른 예전 감성의 SNS.
- 언제 어디서든 유저와 관심사 태그를 검색 해서 정보를 얻을 수있습니다.
- 부담없이 상대방의 정보와 카페목록을 보고 들어갈 수 있습니다.

### 😍 취미를 사용자들끼리 공유하는 SNS

- 영화면 영화 드라마면 드라마 운동 게임 등 취미를 서로 공유하는 SNS
- 예전의 싸이 감성을 자극하는 우리들만의 SNS
- 취미 공유와 간단한 SNS로 많은 사용자가 생겨날것으로 예상.