/*
 User API 예시
 */
import axios from "axios";
import VueCookies from "vue-cookies";

const requestLogin = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분

    axios
    .post('http://localhost:8080/lululala/account/login?email='+data["email"]+'&password='+data["password"])
    .then(res => {
        VueCookies.set('lululalatoken', res.data.token, '30m' );
        VueCookies.set('token', res.data.token, '30m' );
  
        VueCookies.set('refresh_token', res.data.refresh_token, '14d' );
        axios.defaults.headers['refresh_token'] = VueCookies.get('refresh_token');
    
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}
const kakaologin = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분

    axios
    .post('http://localhost:8080/lululala/account/kakaologin?email='+data["email"])
    .then(res => {
        VueCookies.set('lululalatoken', res.data.token, '30m' );
        VueCookies.set('token', res.data.token, '30m' );

        VueCookies.set('refresh_token', res.data.refresh_token, '14d' );
        axios.defaults.headers['refresh_token'] = VueCookies.get('refresh_token');

        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}
const getUser = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분

    axios
    .post('http://localhost:8080/lululala/account/tokenuser/'+data["token"])
    .then(res => {
    
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}

export async function refreshToken(data){
    try{
        const token = await axios.post('http://localhost:8080/lululala/account/refresh?refresh_token='+data)
        .then(res=>{
            VueCookies.set('lululalatoken', res.data.token, '10s' );
          
            let refresh_token = VueCookies.get('refresh_token');
          
            return token;
        })
    }catch(err){
        return err;
    }
}
const requestJoin = (data,callback,errorCallback) => {
    //백앤드와 회원가입 통신하는 부분
    //POST
    return axios
    .post('http://localhost:8080/lululala/account/signup',data)
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
     
        errorCallback(res);
    })
}
const requestConfirm = (data,callback,errorCallback) => {
    return axios
    .post('http://localhost:8080/lululala/account/findpasswordmailsend?email='+data["email"])
    .then((res) => {
      
        callback(res);
  
    })
    .catch((res) => {
        errorCallback(res);
    })
}
const Joinmailsend = (data,callback,errorCallback) => {
    return axios
    .post('http://localhost:8080/lululala/account/mailsend?email='+data["email"])
    .then((res) => {
      
        callback(res);
  
    })
    .catch((res) => {
        errorCallback(res);
    })
}
const requestUpdate = (data,callback,errorCallback) => { 
   
    return axios
    .put('http://localhost:8080/lululala/account/update',data)
    .then((res) => {
    
        callback(res);
    })
    .catch((res) => {
        errorCallback(res);
    })
}
const requestUserSearch = (data,callback,errorCallback) => {        //id로 회원정보
    return axios        
    .post('http://localhost:8080/lululala/account/findUserByEmail/' + data["email"])
    .then((res)=>{
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })
}
const requestUserInfo = (data, callback, errorCallback) => {

    return axios
    .post('http://localhost:8080/lululala/account/findUserByNickname/' + data)
    .then((res)=>{
        callback(res);
    })
    .catch((res) =>{
        errorCallback(res);
    })
}
const requestNicknameCheck = (data, callback, errorCallback) => {       //닉네임 중복확인
    return axios
    .post('http://localhost:8080/lululala/account/searchNickname/' + data["nickname"])
    .then((res)=>{
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })
}
const requestDelete = (data, callback, errorCallback) => {
    return axios
    .delete('http://localhost:8080/lululala/account/delete')
    .then((res)=>{
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })
}
const requestFollower = (data, callback, errorCallback) => {

    return axios
    .post('http://localhost:8080/lululala/account/searchFollower/'+ data)
    .then((res)=> {
        callback(res);
    })
    .catch((res)=> {
        errorCallback(res);
    })
}

const requestFollowing = (data, callback, errorCallback) => {
   
    return axios
    .post('http://localhost:8080/lululala/account/searchFollowing/'+ data)
    .then((res)=> {
        callback(res);
    })
    .catch((res)=> {
        errorCallback(res);
    })
}
const requestUserByUid = (data, callback, errorCallback) => {
    return axios
    .post('http://localhost:8080/lululala/account/searchFollower/'+ data)
    .then((res)=> {
        callback(res);
    })
    .catch((res)=> {
        errorCallback(res);
    })
}
const insertFollow = (data, callback, errorCallback) => {
    return axios
    .post('http://localhost:8080/lululala/account/insertfollow', data)
    .then((res)=> {
        callback(res);
    })
    .catch((res)=> {
        errorCallback(res);
    })
}

const deleteFollow = (data, callback, errorCallback) => {

    return axios
    .post('http://localhost:8080/lululala/account/deletefollow', data)
    .then((res)=> {
        callback(res);
    })
    .catch((res)=> {
        errorCallback(res);
    })
}

const findUserByUid = (data, callback, errorCallback) => {
    return axios
    .post('http://localhost:8080/lululala/account/findUserByUid/'+ data)
    .then((res)=> {
        callback(res);
    })
    .catch((res)=> {
        errorCallback(res);
    })
}

const insertQuration = (data, callback, errorCallback) => {
    
    return axios
    .post('http://localhost:8080/lululala/quration/insertQuration/', data)
    .then((res)=> {
        callback(res);
    })
    .catch((res)=> {
        errorCallback(res);
    })
}

const searchAllCuration = (data, callback, errorCallback) => {
    
    return axios
    .get('http://localhost:8080/lululala/quration/searchAllQuration/'+ data["uid"])
    .then((res)=> {
        callback(res);
    })
    .catch((res)=> {
        errorCallback(res);
    })
}

const showCuration = (data, callback, errorCallback) => {
    
    return axios
    .post('http://localhost:8080/lululala/quration/searchQuration/', data)
    .then((res)=> {
        callback(res);
    })
    .catch((res)=> {
        errorCallback(res);
    })
}

const UserApi = {
    requestLogin:(data,callback,errorCallback)=>requestLogin(data,callback,errorCallback),
    requestJoin:(data,callback,errorCallback)=>requestJoin(data,callback,errorCallback),
    requestConfirm:(data,callback,errorCallback)=>requestConfirm(data,callback,errorCallback),
    requestUpdate:(data,callback,errorCallback)=>requestUpdate(data,callback,errorCallback),
    requestUserSearch:(data,callback,errorCallback)=>requestUserSearch(data,callback,errorCallback),
    requestUserInfo:(data,callback,errorCallback)=>requestUserInfo(data,callback,errorCallback),
    requestUserByUid:(data,callback,errorCallback)=>requestUserByUid(data,callback,errorCallback),
    requestNicknameCheck:(data,callback,errorCallback)=>requestNicknameCheck(data,callback,errorCallback),
    requestDelete:(data,callback,errorCallback)=>requestDelete(data,callback,errorCallback),
    getUser:(data,callback,errorCallback)=>getUser(data,callback,errorCallback),
    requestFollower:(data,callback,errorCallback)=>requestFollower(data,callback,errorCallback),
    requestFollowing:(data,callback,errorCallback)=>requestFollowing(data,callback,errorCallback),
    insertFollow:(data,callback,errorCallback)=>insertFollow(data,callback,errorCallback),
    deleteFollow:(data,callback,errorCallback)=>deleteFollow(data,callback,errorCallback),
    kakaologin:(data,callback,errorCallback)=>kakaologin(data,callback,errorCallback),
    findUserByUid:(data,callback,errorCallback)=>findUserByUid(data,callback,errorCallback),
    insertQuration:(data,callback,errorCallback)=>insertQuration(data,callback,errorCallback),
    searchAllCuration:(data,callback,errorCallback)=>searchAllCuration(data,callback,errorCallback),
    showCuration:(data,callback,errorCallback)=>showCuration(data,callback,errorCallback),
    Joinmailsend:(data,callback,errorCallback)=>Joinmailsend(data,callback,errorCallback),
}

export default UserApi
