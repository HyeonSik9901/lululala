import axios from 'axios'

const insertComment =  (data,callback,errorCallback) => {
    axios
    .post('http://localhost:8080/lululala/comment/insertComment', data)
    .then(res=>{
        callback(res);
    })
    .catch(res=>{
        errorCallback(res);
    })
}

const searchComment = (data,callback,errorCallback) => {
    axios
    .post('http://localhost:8080/lululala/comment/searchComment/' + data)
    .then(res=>{
        callback(res);
    })
    .catch(res=>{
        errorCallback(res);
    })
}

const CommentApi = {
    insertComment:(data,callback,errorCallback)=>insertComment(data,callback,errorCallback),
    searchComment:(data,callback,errorCallback)=>searchComment(data,callback,errorCallback)
}

export default CommentApi

