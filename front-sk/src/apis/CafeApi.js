import axios from "axios";

const CafeApi = {
    searchMyCafe:(data,callback,errorCallback)=>searchMyCafe(data,callback,errorCallback) ,
    createCafe:(data,callback,errorCallback)=>createCafe(data,callback,errorCallback) ,
    requestCafeFeed:(data,callback,errorCallback)=>requestCafeFeed(data,callback,errorCallback) ,
    updateCafeFeed:(data,callback,errorCallback)=>updateCafeFeed(data,callback,errorCallback) ,
    insertCafeFeed:(data,callback,errorCallback)=>insertCafeFeed(data,callback,errorCallback) ,
    deleteCafeFeed:(data,callback,errorCallback)=>deleteCafeFeed(data,callback,errorCallback) ,
    insertCafeComment:(data,callback,errorCallback)=>insertCafeComment(data,callback,errorCallback) ,
    searchCafeComment:(data,callback,errorCallback)=>searchCafeComment(data,callback,errorCallback) ,
    updateCafeComment:(data,callback,errorCallback)=>updateCafeComment(data,callback,errorCallback) ,
    deleteCafeComment:(data,callback,errorCallback)=>deleteCafeComment(data,callback,errorCallback) ,
    insertCafeUser:(data,callback,errorCallback)=>insertCafeUser(data,callback,errorCallback) ,
    //insertCafeUser
}

export default CafeApi


const insertCafeUser = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
  
    return axios
    .post('http://localhost:8080/lululala/insertCafeUser/',data)
    .then(res => {
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}




const searchMyCafe = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
  

    return axios
    .post('http://localhost:8080/lululala/cafe/searchMyCafe/'+data['uid'])
    .then(res => {
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}


const createCafe = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
 

    return axios
    .post('http://localhost:8080/lululala/cafe/insertCafe/', data)
    .then(res => {
    
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}
const requestCafeFeed = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
    
    axios
    .post('http://localhost:8080/lululala/cafefeed/searchAllCafeFeed/'+data["cafeid"])
    .then(res => {
     
        callback(res);
    })
    .catch((res)=>{
     

        errorCallback(res);
    })

}

const insertCafeFeed = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
   
    
    return axios
    .post('http://localhost:8080/lululala/cafefeed/insertCafeFeed'+data["uid"])
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
       
        errorCallback(res);
    })
}
const updateCafeFeed = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분

    
    return axios
    .post('http://localhost:8080/lululala/cafefeed/updateCafeFeed',data)
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
       
        errorCallback(res);
    })
}

const deleteCafeFeed = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
   
    
    return axios
    .delete('http://localhost:8080/lululala/cafefeed/deleteCafeFeed/'+data["cfid"])
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
        
        errorCallback(res);
    })
}

const insertCafeComment = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
  
    
    return axios
    .post('http://localhost:8080/lululala/cafecomment/insertCafeComment', data)
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
        
        errorCallback(res);
    })
}

const updateCafeComment = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
   
    
    return axios
    .post('http://localhost:8080/lululala/cafefeed/updateCafeFeed',data)
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
       
        errorCallback(res);
    })
}
const searchCafeComment = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분

    
    return axios
    .post('http://localhost:8080/lululala/cafecomment/searchCafeComment/'+data["cfid"])
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
     
        errorCallback(res);
    })
}
const deleteCafeComment = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분

    
    return axios
    .delete('http://localhost:8080/lululala/cafecomment/deleteCafeComment/'+data["cid"])
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
      
        errorCallback(res);
    })
}

