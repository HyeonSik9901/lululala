import axios from "axios";

const FeedApi = {
    requestFeed:(data,callback,errorCallback)=>requestFeed(data,callback,errorCallback) ,
    requestmyFeed:(data,callback,errorCallback)=>requestmyFeed(data,callback,errorCallback) ,
    requestlikeFeed:(data,callback,errorCallback)=>requestlikeFeed(data,callback,errorCallback) ,
    createFeed:(data,callback,errorCallback)=>createFeed(data,callback,errorCallback) ,
    requestLikes:(data,callback,errorCallback)=>requestLikes(data,callback,errorCallback),
    checklike:(data,callback,errorCallback)=>checklike(data,callback,errorCallback),
    insertlike:(data,callback,errorCallback)=>insertlike(data,callback,errorCallback),
    deletelike:(data,callback,errorCallback)=>deletelike(data,callback,errorCallback),
    countlike:(data,callback,errorCallback)=>countlike(data,callback,errorCallback),
    requestTaggedFeed:(data,callback,errorCallback)=>requestTaggedFeed(data,callback,errorCallback),
    requestSearchTag:(data,callback,errorCallback)=>requestSearchTag(data,callback,errorCallback),
    searchFeedByHashtag:(data,callback,errorCallback)=>searchFeedByHashtag(data,callback,errorCallback),
    requestFeedbysearch:(data,callback,errorCallback)=>requestFeedbysearch(data,callback,errorCallback),
}

export default FeedApi

const requestFeed = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
    axios
    .post('http://localhost:8080/lululala/feed/searchAllFeed/'+data["uid"])
    .then(res => {
    
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}
const requestmyFeed = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
    axios
    .post('http://localhost:8080/lululala/feed/searchAllmyFeed/'+data["uid"])
    .then(res => {
   
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}
const requestlikeFeed = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분

    axios
    .post('http://localhost:8080/lululala/feed/searchFeedByLike/'+data["uid"])
    .then(res => {
    
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}
const requestFeedbysearch = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분

    axios
    .post('http://localhost:8080/lululala/feed/searchFeedByKeyword',data)
    .then(res => {
 
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}
const requestLikes = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
    axios
    .post('http://localhost:8080/lululala/feed/LikeCountFeed/'+data["fid"])
    .then(res => {

  
        callback(res);
    })
    .catch((res)=>{
        errorCallback(res);
    })

}



const createFeed = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분

    
    return axios
    .post('http://localhost:8080/lululala/feed/insertFeed',data)
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
  
        errorCallback(res);
    })
}

const checklike = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
   
    
    return axios
    .post('http://localhost:8080/lululala/like/checklikefid',data)
    .then((res) => {
        callback(res);
    })
    .catch((res) => {

        errorCallback(res);
    })
}
const insertlike = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
  
    
    return axios
    .post('http://localhost:8080/lululala/like/insertlikes',data)
    .then((res) => {
  
        callback(res);
    })
    .catch((res) => {
        errorCallback(res);
    })
}

const deletelike = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
    
    
    return axios
    .post('http://localhost:8080/lululala/like/delectlikes',data)
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
        errorCallback(res);
    })
}

const countlike = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
    return axios
    .post('http://localhost:8080/lululala/like/countlikes',data)
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
        errorCallback(res);
    })
}

const requestTaggedFeed = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
    return axios
    .post('http://localhost:8080/lululala/feed/searchFeedByPeopletag/' + data)
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
        errorCallback(res);
    })
}

const requestSearchTag = (data,callback,errorCallback) => {
    return axios
    .post('http://localhost:8080/lululala/feed/searchTagByFid/' + data["fid"])
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
        errorCallback(res);
    })
}
const searchFeedByHashtag = (data,callback,errorCallback) => {
    //백앤드와 로그인 통신하는 부분
    return axios
    .post('http://localhost:8080/lululala/feed/searchFeedByHashtag/' + data)
    .then((res) => {
        callback(res);
    })
    .catch((res) => {
        errorCallback(res);
    })
}