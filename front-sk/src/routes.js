

import Login from './views/user/Login.vue'
import Join from './views/user/Join.vue'
import Components from './views/Components.vue'
import MainFeed from './views/user/MainFeed.vue'
import Profile from './views/user/Profile.vue'
import ChangeInfo from './views/user/ChangeInfo.vue'
import FindPassword from './views/user/FindPassword.vue'
import JoinMailConfirm from './views/user/JoinMailConfirm.vue'
import Hashtag from './views/user/Hashtag.vue'
import LikeFeed from './views/user/LikeFeed.vue'
import UpdateUser from './views/user/UpdateUser.vue'
import UserInfo from './views/user/UserInfo.vue'
import FeedUpload from './views/feed/FeedUpload.vue'
import Curation from './views/user/Curation.vue'
import CurationSet from './views/user/CurationSet.vue'
import PeopleProfile from './views/user/PeopleProfile.vue'
import CafeMain from './views/cafe/CafeMain.vue'
import MyCafe from './views/cafe/MyCafe.vue'
import CafeContent from './views/cafe/CafeContent.vue'
import Posts from './views/cafe/Posts.vue'
import CreatePost from './views/cafe/CreatePost.vue'
import CreateCafe from './views/cafe/CreateCafe.vue'
import UpdatePost from './views/cafe/UpdatePost.vue'
import Quration from './views/user/Quration.vue'
import SeachFeedbytag from './views/user/SeachFeedbytag.vue'
import Lululala1 from './views/cafe/Lululala1.vue'
import Lululala2 from './views/cafe/Lululala2.vue'
import Lululala3 from './views/cafe/Lululala3.vue'
import CafeJoin from './views/cafe/CafeJoin.vue'
export default [

    {
        path : '/',
        name : 'Login',
        component : Login,
        meta : {unauthorized : true}
    },
    {
        path : '/user/join',
        name : 'Join',
        component : Join,
        meta : {unauthorized : true}
    },
    {
        path : '/user/JoinMailConfirm:email',
        name : 'JoinMailConfirm',
        component : JoinMailConfirm,
        props : true,
        meta : {unauthorized : true}

    },
    {
        path : '/components',
        name : 'Components',
        component : Components
    },
    {
        path : '/user/MainFeed',
        name : 'MainFeed',
        component : MainFeed
    },
    {
        path : '/user/Profile',
        name : 'Profile',
        component : Profile
    },
    {
        path : '/user/PeopleProfile/:peopleNickname',
        name : 'PeopleProfile',
        component : PeopleProfile,
        props : true
    },
    {
        path : '/user/ChangeInfo',
        name : 'ChangeInfo',
        component : ChangeInfo
    },
    {
        path : '/user/FindPassword',
        name : 'FindPassword',
        component : FindPassword
    },
    {
        path : '/user/Hashtag',
        name : 'Hashtag',
        component : Hashtag
    },
    {
        path : '/user/LikeFeed',
        name : 'LikeFeed',
        component : LikeFeed
    },
    {
        path : '/user/SeachFeedbytag',
        name : 'SeachFeedbytag',
        component : SeachFeedbytag
    },
    {
        path : '/user/UpdateUser',
        name : 'UpdateUser',
        component : UpdateUser
    },
    {
        path : '/user/UserInfo',
        name : 'UserInfo',
        component : UserInfo
    },
    {    
        path : '/user/Curation',
        name : 'Curation',
        component : Curation
    },
    {    
        path : '/user/CurationSet',
        name : 'CurationSet',
        component : CurationSet
    },
    {
        path : '/feed/FeedUpload',
        name : 'FeedUpload',
        component : FeedUpload
    },
    {
        path : '/cafe/CafeMain',
        name : 'CafeMain',
        component : CafeMain
    },
    {
        path : '/cafe/MyCafe',
        name : 'MyCafe',
        component : MyCafe
    },
    {
        path : '/cafe/CafeContent',
        name : 'CafeContent',
        component : CafeContent
    },
    {
        path : '/cafe/Posts',
        name : 'Posts',
        component : Posts
    },
    {
        path : '/cafe/CreatePost',
        name : 'CreatePost',
        component : CreatePost
    },
    {
        path : '/cafe/CreateCafe',
        name : 'CreateCafe',
        component : CreateCafe
    },
    {
        path : '/cafe/UpdatePost',
        name : 'UpdatePost',
        component : UpdatePost
    },
    {    
        path : '/user/Quration',
        name : 'Quration',
        component : Quration
    },
    {    
        path : '/cafe/Lululala1',
        name : 'Lululala1',
        component : Lululala1
    },
    {    
        path : '/cafe/Lululala2',
        name : 'Lululala2',
        component : Lululala2
    },
    {    
        path : '/cafe/Lululala3',
        name : 'Lululala3',
        component : Lululala3
    },
    {    
        path : '/cafe/CafeJoin',
        name : 'CafeJoin',
        component : CafeJoin
    },
    
]
