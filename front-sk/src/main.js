import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router'
import routes from './routes'
import store from './vuex/store'
import 'swiper/dist/css/swiper.css'
import VueCookies from 'vue-cookies'
import VueSession from 'vue-session'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm'
import {refreshToken} from './apis/UserApi'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueMaterial from 'vue-material'
Vue.use(VueMaterial)
Vue.use(BootstrapVue);
import VueAwesomeSwiper from 'vue-awesome-swiper' //사이드뷰 추가
Vue.use(VueAwesomeSwiper)

import VueTabs from 'vue-nav-tabs' //탭 기능 추가
import 'vue-nav-tabs/themes/vue-tabs.css'
Vue.use(VueTabs)

import VueEmoji from 'emoji-vue'
Vue.use(VueEmoji)

Vue.config.productionTip = false

Vue.use(Router)


var SocialSharing = require('vue-social-sharing');
Vue.use(SocialSharing);

const router = new Router({
    mode : 'history',
    routes,
});

// import VueCookie from 'vue-cookies';
// import axios from 'axios';

// const token = VueCookie.get('token');
// if (token) axios.defaults.headers.common.Authorization = VueCookie.get('token');
// axios.interceptors.response.use((res) => {
//   if (res.data.token) {
//     VueCookie.set('token', res.data.token, { expires: '30m' });
//     VueCookie.set('refresh_token', res.data.refresh_token, { expires: '336h' });
//     axios.defaults.headers.common.Authorization = VueCookie.get('token');
//   }
//   return Promise.resolve(res);
// }, (err) => {
//   if (err.response.status === 401) {
//     location.href = '/#/sign';
//     return;
//   }
//   return Promise.reject(err);
// });

// router.beforeEach(async(to, from, next) =>{
//     if(VueCookies.get('token') == null && VueCookies.get('refresh_token') !== null){
//         let refresh_token = VueCookies.get('refresh_token');
//         console.log("안녕 여기야 이번엔")
//         console.log(refresh_token);
//         let data = refresh_token;
//         await refreshToken(data);
//         console.log('토큰 재 요청 후', VueCookies.get('token'));
//     }
//     if (to.matched.some(record => record.meta.unauthorized) || VueCookies.get('token')){
//         console.log('여기들어왔다뤼이이이!!')
//         console.log(VueCookies.get('token'));
//         return next();
//       }
//     if(VueCookies.get('token') === null && VueCookies.get('refresh_token') === null){
//         console.log(VueCookies.get('token'));
//         alert('로그인 시간이 만료되었습니다.');
//         return next('/');
//     }
// })
var options = {
    persist : true
}

Vue.use(VueSession, options)

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');


